// const numbers = [1, 2, 3];
// console.log(numbers);

// // const moreNumbers = Array(5, 2);
// // console.log(moreNumbers);

// // const yetMoreNumbers = Array.of(1, 2);
// // console.log(yetMoreNumbers);

// const listItems = document.querySelectorAll('li');
// console.log(listItems);

// const arrayListItems = Array.from(listItems);
// console.log(arrayListItems);

// const hobbies = ['Tennis', 'Coding'];
// hobbies.push('Running');
// hobbies.unshift('Reading');
// hobbies.splice(1,0,'Good wine')
// console.log(hobbies);

const personData = [{ name: 'Alex', age: 34 }, { name: 'Sara', age: 33 }, { name: 'Hank', age: 8 }];

const hank = personData.find((name, index, personData) => {
    if (name.name === 'Hank') {
        console.log(personData)
        return index;
    }
});

console.log(hank)