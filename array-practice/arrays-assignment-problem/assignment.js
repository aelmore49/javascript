

const numbers = [1, 2, 3, 4, 5, 6, 420, 666, 69];

const filteredNums = numbers.filter(el => el > 5);
const mappedNum = filteredNums.map((el) => {
    let obj = { prop: '' };
    obj.prop = el
    return obj
});

const reduced = numbers.reduce((prev, current) => {
    let num = prev * current;
    return num;
});

const findMax = (args)=>{
    let largest = Math.max(...args);
    let smallest = Math.min(...args);
    let newArray = [largest,smallest]
    return largest
};

const findMaxandMin = (args)=>{
    let largest = Math.max(...args);
    let smallest = Math.min(...args);
    let newArray = [largest,smallest]
    return newArray
};

console.log(filteredNums);
console.log(mappedNum);
console.log(reduced);
console.log(findMax(numbers));
const [large,small] = findMaxandMin(numbers);
console.log(small);
console.log(large) 