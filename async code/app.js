let button = document.querySelector('button');
const output = document.querySelector('p');

const getPosition = (opts) => {
  const promise = new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      },
      opts
    )
  })
  return promise;
}

const setTimer = (duration) => {
  const promise = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('Done!');
    }, duration)
  })
  return promise;
}

async function trackUserHandler() {
  // let positionData;
  let posData;
  let timerData;
  try {
    posData = await getPosition();
    timerData = await setTimer(2000);
  } catch (error) {
    console.log(error);
  }
  console.log(timerData, posData)

  // .then(posData => {
  //   positionData = posData;
  //   return setTimer(2000);
  // })
  // .catch(error => {
  //   console.log(error)
  //   return 'We shall continue'
  // })
  // .then(data => {
  //   console.log(data, positionData)
  // });
  // setTimer(3000).then(data => {
  //   console.log('Finished at last!')
  // });
  // console.log('Getting current position...')
}

button.addEventListener('click', trackUserHandler);


// var promise = job1();

// promise

//   .then(function (data1) {
//     console.log('data1', data1);
//     return job2();
//   })

//   .then(function (data2) {
//     console.log('data2', data2);
//     return 'Hello world';
//   })

//   .then(function (data3) {
//     console.log('data3', data3);
//   });

// function job1() {
//   return new Promise(function (resolve, reject) {
//     setTimeout(function () {
//       resolve('result of job 1');
//     }, 1000);
//   });
// }

// function job2() {
//   return new Promise(function (resolve, reject) {
//     setTimeout(function () {
//       resolve('result of job 2');
//     }, 1000);
//   });
// }
// new Promise((resolve, reject) => {
//   console.log('Initial');

//   resolve();
// })
// .then(() => {
//   throw new Error('Something failed');
      
//   console.log('Do this');
// })
// .catch(() => {
//   console.error('Do that');
// })
// .then(() => {
//   console.log('Do this, no matter what happened before');
// });

// Promise.race([getPosition(),setTimer()]).then(data=>{
//   console.log(data)
// })

// Promise.all([getPosition(),setTimer()]).then(data=>{
//   console.log(data)
// });

Promise.allSettled([getPosition(),setTimer()]).then(data=>{
    console.log(data)
   });
  