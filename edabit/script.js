//6-24-20//
// ARRAYS

/*<----------------------------------------------------------------------------->*/

// //Write a function that retrieves the last n elements from an array.
// function last(a, n) {
//     if (n > a.length) {
//         return "invalid";
//     }
//     return a.slice(a.length - n);
// }
// //console.log(last([1, 2, 3, 4, 5], 3))

/*<----------------------------------------------------------------------------->*/

// //Create a function that applies a discount d to every number in the array.
// function getDiscounts(nums, d) {
//     let discount = parseFloat(d) / 100
//     console.log(discount)
//     let saleArray = nums.map((a) => {
//         let item = (a * discount)
//         return item;
//     })
//     return saleArray
// }
// //console.log(getDiscounts([10, 20, 40, 80], "75%"))

/*<----------------------------------------------------------------------------->*/

// //Create a function that takes a
// //string of lowercase characters and returns that string reversed and in upper case.
// function reverseCapitalize(str) {
//     let altString = str.toUpperCase()
//     let splitString = altString.split("")
//     let arr = splitString.reverse()
//     altString = arr.join("");
//     return altString
// }
// //console.log(reverseCapitalize("edabit"))

/*<----------------------------------------------------------------------------->*/
// /*A word has been split into a left part and a right part.
// Re-form the word by adding both halves together,
//  changing the first character to an uppercase letter.*/
// function getWord(left, right) {
//     let str = left.concat(right)
//     let replace = str.replace(str[0], str[0].toUpperCase())
//     return replace;
// }
// //console.log(getWord("maga", "zine"))

/*<----------------------------------------------------------------------------->*/

// /*You will be given an array of drinks, with each drink being an object with two properties:
//  name and price. Create a function that has the drinks array as an argument and return the drinks
//   object sorted by price in ascending order. */
// const drinks1 = [{
//         name: 'water',
//         price: 120
//     },
//     {
//         name: 'lime',
//         price: 80
//     },
//     {
//         name: 'peach',
//         price: 90
//     },

// ];

// function sortDrinkByPrice(drinks) {
//     let sort = drinks.sort((a, b) => {
//         return a.price - b.price
//     })
//     return sort;
// }
// //console.log(sortDrinkByPrice(drinks1))

/*<----------------------------------------------------------------------------->*/

// /*This is a reverse coding challenge. Here, you must generate your own function to satisfy
// the relationship between the inputs and outputs.Your task is to create a function that,
//  when fed the inputs below, produce the sample outputs shown.

// [5, 7, 8, 2, 1], 2 ➞ [1, 1, 0, 0, 1]

// [9, 8, 16, 47], 4 ➞ [1, 0, 0, 3]

// [17, 11, 99, 55, 23, 1], 5 ➞ [2, 1, 4, 0, 3, 1]

// [6, 1], 7 ➞ [6, 1]

// [3, 2, 9], 3 ➞ [0, 2, 0]

// [48, 22, 0, 19, 33, 100], 10 ➞ [8, 2, 0, 9, 3, 0] */

// function mysteryFunc(arr, num) {
//     let altArr = arr.map((a) => {
//         return a % num;
//     })
//     return altArr
// }
// //console.log(mysteryFunc([5, 7, 8, 2, 1], 2))

/*<----------------------------------------------------------------------------->*/

// /*Create a function that takes two arguments (item, times). The first argument (item)
// is the item that needs repeating while the second argument (times) is the number of times
//  the item is to be repeated. Return the result in an array.

// Examples
// repeat("edabit", 3) ➞ ["edabit", "edabit", "edabit"]

// repeat(13, 5) ➞ [13, 13, 13, 13, 13]

// repeat("7", 2) ➞ ["7", "7"]

// repeat(0, 0) ➞ [] */

// function repeat(item, times) {
//     let repeatArray = []
//     for (let i = 0; i < times; i++) {
//         repeatArray.push(item)
//     }
//     return repeatArray
// }
// //console.log(repeat(5, 3))

/*<----------------------------------------------------------------------------->*/

// /*I'm trying to watch some lectures to study for my next exam but
// I keep getting distracted by meme compilations, vine compilations,
// anime, and more on my favorite video platform.

// Your job is to help me by creating a function that takes in a string
//  and checks to see if it contains the following words or phrases:

//     "anime"
//     "meme"
//     "vines"
//     "roasts"
//     "Danny DeVito"

// If it does, return "NO!". Otherwise, return "Safe watching!".
// Examples
// preventDistractions("vines that butter my eggroll") ➞ "NO!"

// preventDistractions("Hot pictures of Danny DeVito") ➞ "NO!"

// preventDistractions("How to ace BC Calculus in 5 Easy Steps") ➞ "Safe watching!"
// */

// function preventDistractions(str) {
//     let noNos = ["anime", "meme", "vines", "roasts", "Danny DeVito"]
//     let no;
//     let yes;
//     for (n of noNos) {
//         if (str.indexOf(n) >= 0) {
//             no = true;
//         } else {
//             yes = true
//         }
//     }
//     if (no) {
//         return 'NO!'
//     }
//     if (yes) {
//         return 'Safe watching!'
//     }
// }
// //console.log(preventDistractions("Hot pictures of Danny DeVito"))

/*<----------------------------------------------------------------------------->*/

// /*Create a function that takes a string and returns true or false,
//  depending on whether the characters are in order or not.
//  Examples
// isInOrder("abc") ➞ true

// isInOrder("edabit") ➞ false

// isInOrder("123") ➞ true

// isInOrder("xyzz") ➞ true */

// function isInOrder(str) {
//     let strArr = str.split('');
//     let forwardArray = strArr.sort()
//     let forwardString = forwardArray.join('')
//     let backwordString = [...forwardString].reverse().join('');
//     if ((str === forwardString || str === backwordString) && isNaN(str)) {
//         return true
//     }
//     if ((str === forwardString) && (str !== backwordString) && !isNaN(str)) {
//         return true
//     } else {
//         return false
//     }

// }
// //console.log(isInOrder("321"))

/*<----------------------------------------------------------------------------->*/

// /*Create a function that converts two arrays of x- and y- coordinates into
// an array of (x,y) coordinates.
// Examples

// convertCartesian([1, 5, 3, 3, 4], [5, 8, 9, 1, 0])
// ➞ [[1, 5], [5, 8], [3, 9], [3, 1], [4, 0]]

// convertCartesian([9, 8, 3], [1, 1, 1])
// ➞ [[9, 1], [8, 1], [3, 1]]*/

// function convertCartesian(x, y) {
//     let xYcords = [];
//     let xCords = x.map((a) => {
//         return a;
//     })
//     let yCords = y.map((a) => {
//         return a;
//     })

//     for (let i = 0; i < x.length; i++) {
//         xYcords.push([xCords[i], yCords[i]])
//     }
//     return xYcords

// }
// //console.log(convertCartesian([1, 5, 3, 3, 4], [5, 8, 9, 1, 0]))

/*<----------------------------------------------------------------------------->*/

// //6-25-20//

// /*Wild Roger is participating in a Western Showdown, meaning he
//  has to draw (pull out and shoot) his gun faster than his opponent in a gun standoff.
// Given two strings,p1 and p2, return which person drew their gun the fastest.
//  If both are drawn at the same time, return "tie".
//  Examples
// showdown(
//   "   Bang!        ",
//   "        Bang!   "
// ) ➞ "p1"

// // p1 draws his gun sooner than p2

// showdown(
//   "               Bang! ",
//   "             Bang!   "
// ) ➞ "p2"

// showdown(
//   "     Bang!   ",
//   "     Bang!   "
// ) ➞ "tie"*/

// function showdown(p1, p2) {
//     let p1Shot = p1.indexOf('Bang!')
//     let p2Shot = p2.indexOf('Bang!')
//     if (p1Shot < p2Shot) {
//         return 'p1'
//     } else if (p1Shot > p2Shot) {
//         return 'p2'
//     } else {
//         return 'tie'
//     }
// }
// // console.log(showdown(
// //     "   Bang!        ",
// //     "        Bang!   "
// // ))

/*<----------------------------------------------------------------------------->*/

// /*Write a function that maps files to their extension names.
// Examples
// getExtension(["code.html", "code.css"])
// ➞ ["html", "css"]

// getExtension(["project1.jpg", "project1.pdf", "project1.mp3"])
// ➞ ["jpg", "pdf", "mp3"]

// getExtension(["ruby.rb", "cplusplus.cpp", "python.py", "javascript.js"])
// ➞ ["rb", "cpp", "py", "js"] */

// function getExtension(arr) {
//     let ext = arr.map((e) => {
//         let arr = e.split('.').pop()
//         return arr
//     })
//     return ext
// }
// //console.log(getExtension(["project1.jpg", "project1.pdf", "project1.mp3"]))

/*<----------------------------------------------------------------------------->*/

// /*A number added with its additive inverse equals zero. Create a function that
//  returns an array of additive inverses.
//  Examples

// additiveInverse([5, -7, 8, 3]) ➞ [-5, 7, -8, -3]

// additiveInverse([1, 1, 1, 1, 1]) ➞ [-1, -1, -1, -1, -1]

// additiveInverse([-5, -25, 35]) ➞ [5, 25, -35]*/

// function additiveInverse(arr) {
//     let add = arr.map((a) => {
//         if (Math.sign(a) === -1 || Math.sign(a) === -0) {
//             return Math.abs(a)
//         } else if (Math.sign(a) === 1 || Math.sign(a) === 0) {
//             return ~a + 1;
//         }
//     })
//     return add;
// }
// //console.log(additiveInverse([1, 1, 1, 1, 1]))

/*<----------------------------------------------------------------------------->*/

// /*Write a function that returns true if the product of an array is
//  divisible by the sum of that same array. Otherwise, return false.
//  Examples

// divisible([3, 2, 4, 2]) ➞ false

// divisible([4, 2, 6]) ➞ true
// // 4 * 2 * 6 / 4 + 2 + 6

// divisible([3, 5, 1]) ➞ false */

// function divisible(arr) {
//     let a1 = arr.reduce((prev, curr) => {
//         return prev * curr
//     })
//     let a2 = arr.reduce((prev, curr) => {
//         return prev + curr
//     })
//     return a1 % a2 === 0 ? true : false
// }

// //console.log(divisible([4, 4, 4, 4]))

/*<----------------------------------------------------------------------------->*/

// /*Given a string of letters, how many capital letters are there?
// Examples

// capitalLetters("fvLzpxmgXSDrobbgMVrc") ➞ 6

// capitalLetters("JMZWCneOTFLWYwBWxyFw") ➞ 14

// capitalLetters("mqeytbbjwqemcdrdsyvq") ➞ 0 */

// function capitalLetters(str) {
//     let count = 0;
//     for (var i = 0; i < str.length; i++) {
//         if (str[i] === str[i].toUpperCase()) {
//             count += 1
//         }
//     }
//     return count
// }

// //console.log(capitalLetters("nrrvrXlmfwjwlbcjwrzt"))

/*<----------------------------------------------------------------------------->*/

// /*
// Write a function that partitions the array into two subarrays:
// one with all even integers, and the other with all odd integers.
//  Return your result in the following format:
//  Examples

// evenOddPartition([5, 8, 9, 2, 0]) ➞ [[8, 2, 0], [5, 9]]

// evenOddPartition([1, 0, 1, 0, 1, 0]) ➞ [[0, 0, 0], [1, 1, 1]]

// evenOddPartition([1, 3, 5, 7, 9]) ➞ [[], [1, 3, 5, 7, 9]]

// evenOddPartition([]) ➞ [[], []]
// */

// function evenOddPartition(arr) {
//     let subArr = [
//         [],
//         []
//     ]
//     console.log(subArr[0])
//     let findEorO = arr.map((a) => {
//         return a % 2 === 0 ? subArr[0].push(a) : subArr[1].push(a)
//     })
//     return subArr
// }
// //console.log(evenOddPartition([5, 8, 9, 2, 0]))

/*<----------------------------------------------------------------------------->*/

// /*Create a function that replaces all the vowels in a string
//  with a specified character.
//  Examples

// replaceVowels("the aardvark", "#") ➞ "th# ##rdv#rk"

// replaceVowels("minnie mouse", "?") ➞ "m?nn?? m??s?"

// replaceVowels("shakespeare", "*") ➞ "sh*k*sp**r*"*/

// function replaceVowels(str, ch) {
//     const regex = /[aeiou]/gi;
//     return str.replace(regex, ch)
// }
// //console.log(replaceVowels("the aardvark", "#"))

/*<----------------------------------------------------------------------------->*/

// /*
// Write a function that turns a comma-delimited list into an array of strings.
// Examples

// toArray("watermelon, raspberry, orange")
// ➞ ["watermelon", "raspberry", "orange"]

// toArray("x1, x2, x3, x4, x5")
// ➞ ["x1", "x2", "x3", "x4", "x5"]

// toArray("a, b, c, d")
// ➞ ["a", "b", "c", "d"]

// toArray("")
// ➞ [] */

// function toArray(str) {
//     let arr = str.split(', ');
//     return arr[0] == "" ? [] : arr
// }
// //console.log(toArray(""))

/*<----------------------------------------------------------------------------->*/

// /*Hamming distance is the number of characters that differ between two strings.
// Examples

// hammingDistance("abcde", "bcdef") ➞ 5

// hammingDistance("abcde", "abcde") ➞ 0

// hammingDistance("strong", "strung") ➞ 1 */

// function hammingDistance(str1, str2) {
//     let diff = 0;
//     for (var i = 0; i < str1.length; i++) {
//         console.log(str2[i])
//         console.log(str1[i])
//         str2[i] !== str1[i] ? diff += 1 : diff = diff
//     }
//     return diff
// }
// //console.log(hammingDistance("abcde", "bcdef"))

/*<----------------------------------------------------------------------------->*/

// /*Given an array, transform that array into a mirror.
// Examples

// mirror([0, 2, 4, 6]) ➞ [0, 2, 4, 6, 4, 2, 0]

// mirror([1, 2, 3, 4, 5]) ➞ [1, 2, 3, 4, 5, 4, 3, 2, 1]

// mirror([3, 5, 6, 7, 8]) ➞ [3, 5, 6, 7, 8, 7, 6, 5, 3] */

// function mirror(arr) {
//     let mirror = arr.slice().reverse().map((a) => {
//         return a
//     })
//     mirror.shift()
//     let combine = arr.concat(mirror)
//     console.log(combine)
//     return combine;
// }
// //console.log(mirror([0,2,4,6]))

/*<----------------------------------------------------------------------------->*/

// /*
// Create a function that repeats each character in a string n times.
//  Examples

// repeat("mice", 5) ➞ "mmmmmiiiiiccccceeeee"

// repeat("hello", 3) ➞ "hhheeellllllooo"

// repeat("stop", 1) ➞ "stop"*/

// function repeat(str, n) {
//     let arr = str.split('')
//     let newArr = []
//     let finalString;
//     arr.map((a) => {
//         let repeat = a.repeat(n)
//         newArr.push(repeat)
//     })
//     return finalString = newArr.join('')
// }
// //console.log(repeat("hello", 3))

/*<----------------------------------------------------------------------------->*/

// /*We can assign variables by the same name properties of objects, but
// what if I wanted to assign obj.one to a different name like anotherOne?
// Use ES6 object destructuring to assign obj.one to the variable anotherOne.
//  Variable two needs to remain assigned to obj.two. Ignore the .toString()
//   function (used for validation). */

// let str = `({ one:anotherOne, two } = { one : 1, two : 2}).toString()`

/*<----------------------------------------------------------------------------->*/

// /*Create a function that returns the product of all odd integers in an array.
// Examples

// oddProduct([3, 4, 1, 1, 5]) ➞ 15

// oddProduct([5, 5, 8, 2, 4, 32]) ➞ 25

// oddProduct([1, 2, 1, 2, 1, 2, 1, 2]) ➞ 1

// */
// function oddProduct(arr) {
//     let finArr = [];
//     for (var i = 0; i < arr.length; i++) {
//         if (arr[i] % 2 === 1) {
//             finArr.push(arr[i])
//         }
//     }
//     return finArr.reduce((a, b) => {
//         return a * b
//     })
// }
// //console.log(oddProduct([3, 4, 1, 1, 5]))

/*<----------------------------------------------------------------------------->*/

// /*Given a class for a BasicPlan, write the classes for StandardPlan
//  and PremiumPlan which have class properties of the following:
// BasicPlan	StandardPlan	PremiumPlan
// ✓	✓	✓	canStream
// ✓	✓	✓	canDownload
// ✓	✓	✓	hasSD
// 	✓	✓	hasHD
// 		✓	hasUHD
// 1	2	4	numOfDevices
// $8.99	$12.99	$15.99	price
// Examples

// BasicPlan.hasSD ➞ true

// PremiumPlan.hasSD ➞ true

// BasicPlan.hasUHD ➞ false

// BasicPlan.price ➞ '$8.99'

// PremiumPlan.numOfDevices ➞ 4 */

// class BasicPlan {
//     static canStream = true;
//     static canDownload = true;
//     static numOfDevices = 1;
//     static hasSD = true;
//     static hasHD = false;
//     static hasUHD = false;
//     static price = '$8.99';
// }

// class PremiumPlan {
//     static canStream = true;
//     static canDownload = true;
//     static numOfDevices = 4;
//     static hasSD = true;
//     static hasHD = true;
//     static hasUHD = true;
//     static price = '$15.99';
// }

// class StandardPlan {
//     static canStream = true;
//     static canDownload = true;
//     static numOfDevices = 2;
//     static hasSD = true;
//     static hasHD = true;
//     static hasUHD = false;
//     static price = '$12.99';
// }

/*<----------------------------------------------------------------------------->*/

// /*Create a function that takes two parameters and,
//  if both parameters are strings, add them as if they were
//  integers or if the two parameters are integers, concatenate them.
// Examples

// stupidAddition(1, 2) ➞ "12"

// stupidAddition("1", "2") ➞ 3

// stupidAddition("1", 2) ➞ null */

// function stupidAddition(a, b) {
//     if (typeof a === 'string' && typeof b === 'string') {
//         let num1 = parseInt(a)
//         let num2 = parseInt(b)
//         return num1 + num2
//     } else if (typeof a === 'number' && typeof b === 'number') {
//         let word = `${String(a)}${String(b)}`
//         return word;
//     } else if (!(typeof a === 'string' && typeof b === 'string') ||
//         !(typeof a === 'number' && typeof b === 'number')) {
//         return null
//     }

// }
// //console.log(stupidAddition(1, 2))
// //console.log(stupidAddition("1", 2))

/*<----------------------------------------------------------------------------->*/

// /*Implement a function that returns an array containing all the consecutive
//  numbers in ascendant order from the given value low up to the given value high (bounds included).
// Examples

// getSequence(1, 5) ➞ [1, 2, 3, 4, 5]

// getSequence(98, 100) ➞ [98, 99, 100]

// getSequence(1000, 1000) ➞ [1000] */

// function getSequence(low, high) {
//     let arr = [];
//     for (var i = low; i <= high; i++) {
//         arr.push(i)
//     }
//     return arr;
// }
// //console.log(getSequence(1, 5))

/*<----------------------------------------------------------------------------->*/

// /*You are given an array with random words but your program doesn't accept words
//  that begin with the capital letter "C". Remove the unaccepted words and return the new array.
//  Examples

// accepted(["Ducks", "Bears",  "Cats"]) ➞ ["Ducks", "Bears"]

// accepted(["cars", "trucks", "planes"] ➞ ["cars", trucks", "planes"]

// accepted(["Cans", "Worms", "Bugs", "Cold", "Beans"]) ➞ ["Worms", "Bugs", "Beans"*/

// function acceptedWords(arr) {
//     let words = arr.filter((a) => {
//         if (!a.startsWith('C')) {
//             return a
//         }
//     })
//     return words
// }
// //console.log(acceptedWords(["Cans", "Worms", "Bugs", "Cold", "Beans"]))

/*<----------------------------------------------------------------------------->*/

// /*Write two functions:
//   firstArg() should return the first parameter passed in.
//   lastArg() should return the last parameter passed in.

// Return undefined if the function takes no parameters.
// Examples

// firstArg(1, 2, 3) ➞ 1

// lastArg(1, 2, 3) ➞ 3

// firstArg(8) ➞ 8

// lastArg(8) ➞ 8 */

// function firstArg(...a) {
//     return a[0]
// }

// function lastArg(...a) {
//     return a[a.length - 1]
// }
// //console.log(firstArg(1, 2, 3))
// //console.log(lastArg(1, 2, 3))

/*<----------------------------------------------------------------------------->*/

// //6/26/20

// /*A museum wants to get rid of some exhibitions. Katya, the interior architect,
//  comes up with a plan to remove the most boring exhibitions. She gives them a rating,
//   and removes the one with the lowest rating. Just as she finishes rating the exhibitions,
//    she's called off to an important meeting. She asks you to write a program that tells her
//     the ratings of the items after the lowest one is removed.

// Create a function that takes an array of integers and removes the smallest value.
// Examples

// removeSmallest([1, 2, 3, 4, 5] ) ➞ [2, 3, 4, 5]

// removeSmallest([5, 3, 2, 1, 4]) ➞ [5, 3, 2, 4]

// removeSmallest([2, 2, 1, 2, 1]) ➞ [2, 2, 2, 1] */
// function removeSmallest(arr) {
//     let smallest = Math.min(...arr);
//     let found = arr.findIndex(el => el == smallest)
//     arr.splice(found, 1)
//     return arr;

// }
// //console.log(removeSmallest([2, 2, 1, 2, 1]))

/*<----------------------------------------------------------------------------->*/

// /*In this exercise you will have to:

//     Take a list of names.
//     Add "Hello" to every name.
//     Make one big string with all greetings.

// The solution should be one string with a comma in between every "Hello (Name)".
// Examples

// greetPeople(["Joe"]) ➞ "Hello Joe"

// greetPeople(["Angela", "Joe"]) ➞ "Hello Angela, Hello Joe"

// greetPeople(["Frank", "Angela", "Joe"]) ➞ "Hello Frank, Hello Angela, Hello  */

// function greetPeople(names) {
//     let sent = names.map((a) => {
//         return `Hello ${a} `
//     })
//     let str = sent.join()
//     return str
// }
// //console.log(greetPeople(["Kyrill"]))

/*<----------------------------------------------------------------------------->*/

// /*Create a function that returns the original value from a matrix with too many sub-arrays.
// Examples

// deNest([[[[[[[[[[[[3]]]]]]]]]]]]) ➞ 3

// deNest([[[[[[[true]]]]]]]) ➞ true

// deNest([[[[[[[[[[[[[[[[["edabit"]]]]]]]]]]]]]]]]]) ➞ "edabit" */

// function deNest(arr) {
//     return arr.flat(Infinity)[0];
// }
// //console.log(deNest([[[[[[[[[[[[3]]]]]]]]]]]]))

/*<----------------------------------------------------------------------------->*/

// /*Create a function which takes an array of instances from
// the class IceCream and returns the sweetness value of the sweetest icecream.
// Sweetness is calculated from the flavor and number of sprinkles.
// Each sprinkle has a sweetness value of 1, and the sweetness values for the flavors are as follows:
// Flavours	Sweetness Value
// Plain	0
// Vanilla	5
// ChocolateChip	5
// Strawberry	10
// Chocolate	10
// You'll be given instance properties in the order flavor, numSprinkles.

// Examples
// ice1 = IceCream("Chocolate", 13)         // value of 23
// ice2 = IceCream("Vanillla", 0)           // value of 5
// ice3 = IceCream("Strawberry", 7)        // value of 17
// ice4 = IceCream("Plain", 18)             // value of 18
// ice5 = IceCream("ChocolateChip", 3)      // value of 8
// sweetestIcecream([ice1, ice2, ice3, ice4, ice5]) ➞ 23
// sweetestIcecream([ice3, ice1]) ➞ 23
// sweetestIcecream([ice3, ice5]) ➞ 17 */

// class IceCream {
//     constructor(flavor, numSprinkles) {
//         this.flavor = flavor
//         this.numSprinkles = numSprinkles
//     }
// }

// function sweetestIcecream(arr) {
//     var sweetVal = {
//         "Plain": 0,
//         "Vanilla": 5,
//         "ChocolateChip": 5,
//         "Strawberry": 10,
//         "Chocolate": 10
//     };
//     var res = arr.map(elem => sweetVal[elem["flavor"]] + elem["numSprinkles"]);
//     return Math.max(...res);
// }
// ice1 = new IceCream("Chocolate", 13)
// ice2 = new IceCream("Vanilla", 0)
// ice3 = new IceCream("Strawberry", 7)
// ice4 = new IceCream("Plain", 18)
// ice5 = new IceCream("ChocolateChip", 3)
// ice6 = new IceCream("Chocolate", 23)
// ice7 = new IceCream("Strawberry", 0)
// ice8 = new IceCream("Plain", 34)
// ice9 = new IceCream("Plain", 81)
// ice10 = new IceCream("Vanilla", 12)
// //console.log(sweetestIcecream([ice1, ice2, ice3, ice4, ice5]))

/*<----------------------------------------------------------------------------->*/

// /*You work in a toy car workshop, and your job is to build toy cars from a collection of parts. Each toy car needs 4 wheels, 1 car body, and 2 figures of people to be placed inside. Given the total number of wheels, car bodies and figures available, how many complete toy cars can you make?
// Examples

// cars(2, 48, 76) ➞ 0
// # 2 wheels, 48 car bodies, 76 figures

// cars(43, 15, 87) ➞ 10

// cars(88, 37, 17) ➞ 8 */

// function cars(wheels, bodies, figures) {
//     let car = []
//     let nesWheels = Math.floor(wheels / 4)
//     let nesBodies = bodies;
//     let nesFigures = Math.floor(figures / 2)
//     car.push(nesBodies, nesFigures, nesWheels)
//     let carAmt = car.sort((a, b) => {
//         return a - b
//     })
//     return carAmt[0]
// }
// //console.log(cars(9, 44, 34))

/*<----------------------------------------------------------------------------->*/

// /*
// Write a function that takes an array of elements and returns only the integers.
// Examples

// returnOnlyInteger([9, 2, "space", "car", "lion", 16]) ➞ [9, 2, 16]

// returnOnlyInteger(["hello", 81, "basketball", 123, "fox"]) ➞ [81, 123]

// returnOnlyInteger([10, "121", 56, 20, "car", 3, "lion"]) ➞ [10, 56, 20, 3]

// returnOnlyInteger(["String",  true,  3.3,  1]) ➞ [1] */

// function returnOnlyInteger(arr) {
//     let returnArr = arr.filter((a) => {
//         if (Number.isInteger(a)) {
//             return a
//         }

//     })
//     return returnArr
// }
// //console.log(returnOnlyInteger([9, 2, "space", "car", "lion", 16]))

/*<----------------------------------------------------------------------------->*/

// /*
// Google's logo can be stretched depending on how many pages it lets you skip forward to.
// Image of Goooooooooogle
// Let's say we wanted to change the amount of pages that Google could skip to.
// Create a function where given a number of pages n, return the word "Google" but with
//  the correct number of "o"s.
// Examples

// googlify(10) ➞ "Goooooooooogle"

// googlify(23) ➞ "Gooooooooooooooooooooooooogle"

// googlify(2) ➞ "Google"

// googlify(-2) ➞ "invalid" */

// function googlify(n) {

//     return n > 1 ? `G${"o".repeat(n)}gle` : "invalid"
// }

// //console.log(googlify(3))

/*<----------------------------------------------------------------------------->*/

// /* 6/28/2020 */

// /*Your function will be passed two functions, f and g, that don't take any parameters.
//  Your function has to call them, and return a string which indicates which function
//  returned the larger number.

//     If f returns the larger number, return the string f.
//     If g returns the larger number, return the string g.
//     If the functions return the same number, return the string neither.

// Examples

// whichIsLarger(() => 5, () => 10) ➞ "g"

// whichIsLarger(() => 25,  () => 25) ➞ "neither"

// whichIsLarger(() => 505050, () => 5050) ➞ "f" */

// function whichIsLarger(f, g) {
//     if (f() > g()) {
//         return 'f';
//     } else if (f() < g()) {
//         return 'g';
//     } else if (f() === g()) {
//         return 'neither';
//     }
// }
// //console.log(whichIsLarger(() => 5, () => 10))

/*<----------------------------------------------------------------------------->*/

// /*Write a function that finds the sum of the first n natural numbers.
//  Make your function recursive.Examples

// sum(5) ➞ 15
// // 1 + 2 + 3 + 4 + 5 = 15

// sum(1) ➞ 1

// sum(12) ➞ 78 */

// function sum(n) {
//     if (n <= 1) {
//         return n
//     }
//     for (var i = 0; i <= n; i++) {
//         return n + sum(n - 1)
//     }
// }
// //console.log(sum(12))

/*<----------------------------------------------------------------------------->*/

// /*Create a function to extract the name of the subreddit from its URL.
// Examples

// subReddit("https://www.reddit.com/r/funny/") ➞ "funny"

// subReddit("https://www.reddit.com/r/relationships/") ➞ "relationships"

// subReddit("https://www.reddit.com/r/mildlyinteresting/") ➞ "mildlyinteresting" */

// function subReddit(link) {
//     let strArr = link.split('/')
//     strArr.pop()
//     return strArr[strArr.length - 1]
// }
// //console.log(subReddit("https://www.reddit.com/r/mildlyinteresting/"))

// /*Write a function that removes any non-letters from a string, returning a well-known
//  film title.
// Examples

// lettersOnly("R!=:~0o0./c&}9k`60=y") ➞ "Rocky"

// lettersOnly("^,]%4B|@56a![0{2m>b1&4i4") ➞ "Bambi"

// lettersOnly("^U)6$22>8p).") ➞ "Up" */

// function lettersOnly(str) {
//     return str.replace(/[^a-z]/gi, '')
// }
// //console.log(lettersOnly('^U)6$22>8p).'))

/*<----------------------------------------------------------------------------->*/

// /*There has been a masterdata issue which affected the unit of measure of the products.
//  All values need to be checked if they are valid. The unit of measure is valid when it
//   is either "L" (liters), "PCE" (pieces) OR when the product has a comment.

// The return value should be a Boolean.
// Expected results

// hasValidUnitOfMeasure({ "product": "Milk", unitOfMeasure: "L" }) ➞ true

// hasValidUnitOfMeasure({ "product": "Cereals", unitOfMeasure: "" }) ➞ false

// hasValidUnitOfMeasure({ "product": "Beer", unitOfMeasure: false }) ➞ false

// hasValidUnitOfMeasure({ "product": "Beef", unitOfMeasure: "Cow"  */

// function hasValidUnitOfMeasure(products = {}) {
//     const {
//         product,
//         unitOfMeasure,
//         comment
//     } = products
//     console.log(products)
//     console.log(products.product)
//     console.log(products.unitOfMeasure)
//     console.log(products.comment)

//     if ((products.unitOfMeasure === 'L' || products.unitOfMeasure === 'PCE') || products.comment) {
//         return true;
//     } else {
//         return false
//     }

// }
// //console.log(hasValidUnitOfMeasure({ "product": "Milk", unitOfMeasure: "L" }))

/*<----------------------------------------------------------------------------->*/

// /*
// I am trying to filter out empty arrays from an array. In other words, I want to
//  transform something that looks like this: ["a", "b", [], [], [1, 2, 3]] to look
//   like ["a", "b", [1, 2, 3]]. My code looks like this:

// function removeEmptyArrays(arr) {
//   return arr.filter(x => x !== [])
// }

// However, somehow, the empty arrays still exist.
// Fix this incorrect code to remove the empty arrays.
// Examples

// // What I want:
// removeEmptyArrays(["a", "b", []]) ➞ ["a", "b"]
// removeEmptyArrays([1, 2, [], 4]) ➞ [1, 2, 4]

// // What I am getting:
// removeEmptyArrays(["a", "b", []]) ➞ ["a", "b", []]
// removeEmptyArrays([1, 2, [], 4]) ➞ [1, 2, [], 4] */

// function removeEmptyArrays(arr) {

//     let fil = arr.filter((a) => {
//         if (typeof a !== 'object') {
//             return a
//         }
//     })
//     return fil
// }
// //console.log(removeEmptyArrays([1, 2, [], 4, 's']))

/*<----------------------------------------------------------------------------->*/

// /*Smash factor is a term in golf that relates to the amount of energy transferred
//  from the club head to the golf ball. The formula for calculating smash factor is
//   ball speed divided by club speed.

// Create a function that takes ball speed bs and club speed cs as arguments and returns
//  the smash factor to the nearest hundredth.
// Examples

// smashFactor(139.4, 93.8) ➞ 1.49

// smashFactor(181.2, 124.5) ➞ 1.46

// smashFactor(154.7, 104.3) ➞ 1.48 */

// function smashFactor(bs, cs) {
//     return parseFloat((bs / cs).toFixed(2))
// }
// //console.log(smashFactor(181.2, 124.5))

/*<----------------------------------------------------------------------------->*/

// /*
// Write a function that returns true if two arrays have the same number of unique elements,
//  and false otherwise.

// To illustrate:

// arr1 = [1, 3, 4, 4, 4]
// arr2 = [2, 5, 7]

// In arr1, the number 4 appears three times, which means it contains three unique elements: [1, 3, 4].
//  Since arr1 and arr2 both contain the same number of unique elements, this example would return true.
// Examples

// same([1, 3, 4, 4, 4], [2, 5, 7]) ➞ true

// same([9, 8, 7, 6], [4, 4, 3, 1]) ➞ false

// same([2], [3, 3, 3, 3, 3]) ➞ true*/

// function same(a1, a2) {
//     let first = 0
//     let second = 0
//     a1.sort((a, b) => {
//         if (a !== b) {
//             first += 1
//         }
//     })
//     a2.sort((a, b) => {
//         if (a !== b) {
//             second += 1
//         }
//     })
//     return first === second ? true : false
// }
// //console.log(same([9, 8, 7, 6], [4, 4, 3, 1]))

/*<----------------------------------------------------------------------------->*/

// /*Write a function that finds the sum of an array. Make your function recursive.
// Examples

// sum([1, 2, 3, 4]) ➞ 10

// sum([1, 2]) ➞ 3

// sum([1]) ➞ 1

// sum([]) ➞ 0 */

// function sum(arr) {
//     if (arr.length === 0) {
//         return 0;
//     } else {
//         return arr[0] + sum(arr.slice(1));
//     }
// }
// //console.log(sum([1, 2, 3, 4]))

// /*----------------6/29/20---------------------*/

// const greeting = greetingMaker("Hello")

// function greetingMaker(salutation) {
//     return function closure(name) {
//         return salutation + ", " + name
//     }
// }
// //console.log(greeting("James"))

/*<----------------------------------------------------------------------------->*/

// /*Create a function that takes a number as an argument and returns the highest
//  digit in that number.
// Examples

// highestDigit(379) ➞ 9

// highestDigit(2) ➞ 2

// highestDigit(377401) ➞ 7 */

// function highestDigit(number) {
//     let numArr = number.toString().split('').reduce((prev, cur) => {
//         return prev > cur ? prev : cur
//     })
//     return parseInt(numArr);
// }
// //console.log(highestDigit(222222))

/*<----------------------------------------------------------------------------->*/

// /*
// Create a function that counts how many characters make up a rectangular shape.
//  You will be given a array of strings.
// Examples
// countCharacters([
//   "###",
//   "###",
//   "###"
// ]) ➞ 9
// countCharacters([
//   "22222222",
//   "22222222",
// ]) ➞ 16
// countCharacters([
//   "------------------"
// ]) ➞ 18
// countCharacters([]) ➞ 0
// countCharacters([ "", ""]) ➞ 0
// */
// function countCharacters(arr) {
//     if (arr.length === 0) {
//         return 0;
//     }
//     for (var i = 0; i < arr.length; i++) {
//         return (arr[i].length * arr.length)
//     }
// }
// //console.log(countCharacters(['', '']))

/*<----------------------------------------------------------------------------->*/

// /*
// The mean of a group of numbers is calculated by summing all numbers, and dividing
// this sum by the total count of numbers in the group.
//  Given a sorted array of numbers, return the mean (rounded to one decimal place).

// */

// function mean(nums) {
//     let tlt = nums.reduce((prev, curr) => {
//         return (prev + curr)
//     }) / nums.length;
//     return parseFloat(tlt.toFixed(1))
// }
// //console.log(mean([1, 6, 6, 7, 8, 8, 9, 10, 10]))

/*<----------------------------------------------------------------------------->*/

// /*
// Write a function that reverses a string. Make your function recursive.
// Examples

// reverse("hello") ➞ "olleh"

// reverse("world") ➞ "dlrow"

// reverse("a") ➞ "a"

// reverse("") ➞ ""
// */
// function reverse(str) {
//     if (str.length === 0) {
//         return ''
//     } else if (str.length === 1) {
//         return str[0]
//     } else {
//         return reverse(str.substr(1)) + str.charAt(0);
//     }
// }

// //console.log(reverse("hello"))

/*<----------------------------------------------------------------------------->*/

// /*
// The time has a format: hours:minutes. Both hours and minutes has two digits,
// like 09:00.
// Make a regexp to find time in the string: Breakfast at 09:00 in the room 123:456.
//  In this task there’s no need to check time correctness
//  yet, so 25:99 can also be a valid result. The regexp should not match 123:456.
// */

// const REGEXP = /\d{2}\:\d{2}/;
// //const str2 = "Breakfast at 09:00 in the room 123:456.";
// const validate = (REGEXP) => {
//     // if(!//.test(String(REGEXP))) return () => "invalid"
//     return function testReg(str2) {
//         return str2.match(REGEXP)
//     }
// }
// const testExp = validate(REGEXP)

/*<----------------------------------------------------------------------------->*/

// /*
// Write a function that returns true if a number is a palindrome.
// Examples

// isPalindrome(838) ➞ true

// isPalindrome(4433) ➞ false

// isPalindrome(443344) ➞ true*/

// function isPalindrome(n) {
//     let str = n.toString();
//     let str2 = Array.from(str).reverse().join('')
//     return str === str2 ? true : false
// }
// //console.log(isPalindrome(13))

/*<----------------------------------------------------------------------------->*/

// /*Something which is not true is false, but something which is not not true is true!
//  Create a function where given n number of "not", evaluate whether it's true or false.
// Examples

// notNotNot(1, true) ➞ false
// // Not true

// notNotNot(2, false) ➞ false
// // Not not false

// notNotNot(6, true) ➞ true
// // Not not not not not not true */

// function notNotNot(n, bool) {
//     return n % 2 == 1 ? !bool : bool;
// }
// //console.log(notNotNot(24, false))

/*<----------------------------------------------------------------------------->*/

// /*
// Create a function that takes a sentence and turns every "i" into "wi" and "e" into
// "we", and add "owo" at the end.
// Examples

// owofied("I'm gonna ride 'til I can't no more")
// ➞ "I'm gonna rwidwe 'twil I can't no morwe owo"

// owofied("Do you ever feel like a plastic bag")
// ➞ "Do you wevwer fwewel lwikwe a plastwic bag owo"

// owofied("Cause baby you're a firework")
// ➞ "Causwe baby you'rwe a fwirwework owo"
// */

// function owofied(sentence) {
//     return sentence.replace(/i/g, "wi").replace(/e/g, "we") + " owo";
// }
// //console.log(owofied("I'm gonna ride 'til I can't no more"))

/*<----------------------------------------------------------------------------->*/

// /*
// You are given one input: An array containing eight 1's and/or 0's. Write a function
//  that takes an 8 bit binary number and convert it to decimal. */

// // function binaryToDecimal(binary) {
// //     console.log(parseInt(binary.join(""), 2))
// //     return parseInt(binary.join(""), 2);
// // }
// //console.log(binaryToDecimal([1, 1, 1, 1, 1, 1, 1, 1]))

// /*----------------6/30/20: start 7:00am------------------ */

// /*Async operations don't always go as planned. When errors creep up we need to know
//  how to handle them. We can pass the reject callback to our executor function to
//  pass errors to our promise.
//  let promise = new Promise( (resolve, reject) => {
//   setTimeout(( ) => {
//     // something went wrong
//     reject('oops!')
// }, 1000)
// })

/*<----------------------------------------------------------------------------->*/

// You can pass Error objects as well. Here we pass a simple string "oops!".

//  */

// // let promise = new Promise((resolve, reject) => {
// //     let animal = "cat"
// //     setTimeout(() => {
// //         if (animal === "dog") {
// //             resolve("It's a dog!")
// //         }
// //         if (animal !== "dog") {

// //             reject(`It's not a dog!`)
// //             /* need something here, you might also need to pass
// //             something else besides the resolve callback */
// //         }
// //     }, 1000)
// // })

/*<----------------------------------------------------------------------------->*/

// /*
// Create a function that takes two parameters (start, stop), and
//  returns the sum of all even numbers in the range.
// */

// // function sumEvenNumsInRange(start, stop) {
// //     let sum = 0
// //     for (var i = start; i <= stop; i++) {
// //         if (i % 2 == 0) {
// //             sum += i
// //         }
// //     }
// //     return sum;
// // }

// // //console.log(sumEvenNumsInRange(51, 150))

/*<----------------------------------------------------------------------------->*/

// // /*Write a function that returns true if an object is empty, and false otherwise. */

// // function isEmpty(obj) {
// //     return Object.keys(obj).length === 0 ? true : false;
// // }

// //isEmpty({})

/*<----------------------------------------------------------------------------->*/

// /*
// Create a function that takes a string as input and capitalizes a letter
//  if its ASCII code is even and returns its lower case version if its ASCII
//   code is odd.

// */
// function asciiCapitalize(str) {
//     return [...str].map(x => x.charCodeAt(0) % 2 ? x.toLowerCase() :
//         x.toUpperCase()).join("")
// }
// //console.log(asciiCapitalize("Oh what a beautiful morning."))

/*<----------------------------------------------------------------------------->*/

// /*Create a method in the Person class which returns how another person's age compares.
//  Given the instances p1, p2 and p3, which will be initialised with the attributes name
//   and age, return a sentence in the following format:
// {other person name} is {older than / younger than / the same age as} me.
// Examples

// p1 = Person("Samuel", 24)
// p2 = Person("Joel", 36)
// p3 = Person("Lily", 24)

// p1.compareAge(p2) ➞ "Joel is older than me."

// p2.compareAge(p1) ➞ "Samuel is younger than me."

// p1.compareAge(p3) ➞ "Lily is the same age as me." */

// // class Person {
// //     constructor(name, age) {
// //         this.name = name;
// //         this.age = age;
// //     }
// //     compareAge(other) {
// //         if (this.age > other.age) {
// //             return `${other.name} is younger than me.`
// //         } else if (this.age === other.age) {
// //             return `${other.name} is the same age as me.`
// //         } else {
// //             return `${other.name} is older than me.`
// //         }

// //     }
// // }

// // p1 = new Person("Samuel", 24)
// // p2 = new Person("Joel", 36)
// // p3 = new Person("Lily", 24)

// //console.log(p1.compareAge(p2))

/*<----------------------------------------------------------------------------->*/

// /*Create a function that takes an array and a string as arguments and returns
// the list of CMSs that include the given string. Return the names in an array in
// alphabetical order.
// Examples
// cmsSelector(["WordPress", "Joomla", "Drupal" ], "w") ➞ ["WordPress"]

// cmsSelector(["WordPress", "Joomla", "Drupal", "Magento" ], "ru") ➞ ["Drupal"]

// cmsSelector(["WordPress", "Joomla", "Drupal", "Magento" ], "") ➞ */

// // // function cmsSelector(arr, str) {
// // //     let newArr = [];
// // //     let check = arr.map((a) => {
// // //         let indx = a.indexOf(str)
// // //         if (a.indexOf(str) !== -1 && str.length > 0) {
// // //             newArr.unshift(arr[a.indexOf(str)])
// // //         } else if (str.length === 0) {
// // //             return newArr = [...arr]
// // //         }
// // //     })
// // //     return newArr.sort()
// // // }
// //console.log(cmsSelector(["WordPress", "Joomla", "Drupal", "Magento", "Shopify", "Blogger"], "er"))
// //console.log(cmsSelector(["WordPress", "Joomla", "Drupal", "Magento", "Shopify", "Blogger"], ""))
// //"['Blogger', 'Drupal', 'Joomla', 'Magento', 'Shopify', 'WordPress']",

/*<----------------------------------------------------------------------------->*/

// /*
// Wild Roger is tasked with shooting down 6 bottles with 6 shots as fast as possible.
//  Here are the different types of shots he could make:

//     He could use one pistol to shoot a bottle with a "Bang!" in 0.5 seconds.
//     Or he could use both pistols at once with a "BangBang!" to shoot two bottles in 0.5 seconds.

// Given an array of strings, return the time (in seconds) it took to shoot down all 6 bottles.
//  Make sure to only count Bangs and BangBangs. Anything else doesn't count.
// Examples

// rogerShots(["Bang!", "Bang!", "Bang!", "Bang!", "Bang!", "Bang!"]) ➞ 3

// rogerShots(["Bang!", "Bang!", "Bang!", "Bang!", "BangBang!"]) ➞ 2.5

// rogerShots(["Bang!", "BangBangBang!", "Boom!", "Bang!", "BangBang!", "BangBang!

// */

// // function rogerShots(arr) {
// //     let altArr = arr.filter((a) => {
// //         if (a == "Bang!" || a == "BangBang!") {
// //             return a
// //         }
// //     })
// //     let calc = altArr.reduce((prev, curr) => {
// //         return altArr.length * 0.5
// //     });
// //     return calc
// // }

// // console.log(rogerShots(["Bang!", "Bang!", "Bang!", "Bang!", "Bang!", "Bang!"]))
// // console.log(rogerShots(["Bang!", "Bang!", "Bang!", "Bang!", "BangBang!"]))
// // console.log(rogerShots(["Bang!", "BangBangBang!", "Boom!", "Bang!", "BangBang!", "BangBang!"]))
// // console.log(rogerShots(["BangBang!", "BangBang!", "BangBang!"]))
// // console.log(rogerShots(["Bang!", "BadaBing!", "Badaboom!", "Bang!", "Bang!", "Bang!", "Bang!", "Bang!"]))
// // console.log(rogerShots(["BangBang!", "BangBang!", "Bag!", "Ban!", "Tang!", "Bang!", "Bang!"]))

// /*----------------8/25/20: start 11:32 am------------------ */

// // Write a function to reverse an array.
// // Examples

// // reverse([1, 2, 3, 4]) ➞ [4, 3, 2, 1]

// // reverse([9, 9, 2, 3, 4]) ➞ [4, 3, 2, 9, 9]

// // reverse([]) ➞ []

// // function reverse(arr) {
// //   return arr.reverse()
// // }
// // reverse([1, 2, 3, 4])

// /*
/*<----------------------------------------------------------------------------->*/

// Create a function to return the amount
//  of potatoes there are in a string.
// Examples

// potatoes("potato") ➞ 1

// potatoes("potatopotato") ➞ 2

// potatoes("potatoapple") ➞ 1

// function potatoes(str) {
// 	let rx = /potato/g;
// 	return str.match(rx).length;
// }

// potatoes("potato")

// */

/*<----------------------------------------------------------------------------->*/
// /*

// Given an array of numbers, return true if the sum
//  of the array is less than 100; otherwise return false.
// Examples

// function arrayLessThan100(arr) {
//     let sum = arr.reduce((a, b) => {
//         return a + b;
//     });
//     if (sum < 100) {
//         return true
//     } else {
//         return false;
//     }
// }
// arrayLessThan100([5, 57])

/*<----------------------------------------------------------------------------->*/
// function gradePercentage(userScore, passScore) {
//     let userS = parseInt(userScore, 10)
//     let passS = parseInt(passScore, 10)
//     let word;

//     if (userS < passS)
//         word = 'FAILED';
//     if (userS >= passS)
//         word = 'PASSED';
//     let sentence = `You ${word} the Exam`;
//     return sentence;
// }
// gradePercentage("85%", "85%");

// gradePercentage("99%", "85%")

// gradePercentage("65%", "90%")

/*<----------------------------------------------------------------------------->*/
// Create a function that takes a number as an argument and
//  returns the square root of that number cubed.
// Examples

// cubeSquareRoot(81) ➞ 729

// cubeSquareRoot(1646089) ➞ 2111932187

// cubeSquareRoot(695556) ➞ 580093704

// function cubeSquareRoot(num) {
//     return Math.sqrt(num) ** 3;
//  }

/*<----------------------------------------------------------------------------->*/

//  Using the .test() method in your function,
//   return whether a string contains the characters
//   "a" and "c" (in that order) with any number of characters
//   (including zero) between them.

//   function asterisk(string) {
// 	return /a.*c/.test(string)
// }

/*<----------------------------------------------------------------------------->*/

// Given two strings, create a function
//  that returns the total number of unique characters
//   from the combined string.

// function countUnique(s1, s2) {
//     // Doing without using sets
//     let uniqueLetters = [];
//     for (let letter of (s1 + s2)) {
//         console.log(letter)
//         if (!uniqueLetters.includes(letter))
//             uniqueLetters.push(letter);
//     }
//     return uniqueLetters.length;
// }

// countUnique("apple", "play")

/*<----------------------------------------------------------------------------->*/

// In this challenge you will be given a relation between two numbers, written as a string.
// Here are some example inputs:

// function isTrue(relation) {
// 	return eval(relation.replace('=', '==='))
// }

// isTrue("8<7")

/*<----------------------------------------------------------------------------->*/

// /*Given an array, return true if there are more odd numbers
//  than even numbers, otherwise return false. */

// // function oddeven(arr) {
// //     let odd = 0;
// //     let even = 0;
// //     let numbers = arr.forEach((a) => {
// //         if (a % 2 == 0) {
// //             even++
// //         } else {
// //             odd++
// //         }
// //     })
// //     if (odd > even) {
// //         return true;
// //     } else {
// //         return false;
// //     }
// // }
// // oddeven([1, 2, 3, 4, 5, 6, 7, 8, 9])

/*<----------------------------------------------------------------------------->*/

// /*
// Create a function that takes two numbers and a mathematical operator and returns the result.
// function calculate(num1, num2, op) {
//     return eval(`${num1} ${op} ${num2}`)
// }
// calculate(24, 100, "-")
// */

/*----------------8/26/20  2:20 PM---------------------*/

// /*
// Create a function that can turn Yen (Japanese dollar) to USD (American dollar).
// Examples

// yenToUsd(1) ➞ 0.01

// yenToUsd(500) ➞ 4.65

// yenToUsd(649) ➞ 6.04

// function yenToUsd(yen) {
//     let usDol = 107.5;
//     let yenToDol = (yen / usDol).toFixed(2);
//     let num = Number.parseFloat(yenToDol)
//     return num;
// }

// yenToUsd(1)
// yenToUsd(500)
// yenToUsd(649)
// yenToUsd(1000)

/*<----------------------------------------------------------------------------->*/

// A word is on the loose and now has tried to hide amongst a crowd
//  of tall letters! Help write a function to detect what the word is, knowing the following rules:
// The wanted word is in lowercase.
// The crowd of letters is all in uppercase.
//  Note that the word will be spread out amongst the random letters,
//   but their letters remain in the same order.

// function detectWord(str) {
//     let strArray = [...str].filter(word => word === word.toLowerCase()).join('')
//     return strArray;
// }
// //detectWord("UcUNFYGaFYFYGtNUH");
// detectWord("YFemHUFBbezFBYzFBYLleGBYEFGBMENTment");

/*<----------------------------------------------------------------------------->*/

// Write a function that takes a year and returns its corresponding century.

// function centuryFromYear(year) {

//     let x = year % 100 === 0 ? year / 100 : Math.floor(year / 100) + 1;
//     return x;
// }
// centuryFromYear(200);

/*<----------------------------------------------------------------------------->*/

// Create a function that returns true if the first array can be nested inside the second.
// arr1 can be nested inside arr2 if:

//     arr1's min is greater than arr2's min.
//     arr1's max is less than arr2's max.

// function canNest(arr1, arr2) {
//     let arr1Max = arr1.reduce((pre, cur) => {
//         return Math.max(pre, cur)
//     })
//     let arr1Min = arr1.reduce((pre, cur) => {
//         return Math.min(pre, cur)
//     })
//     let arr2Max = Math.max(...arr2);
//     let arr2Min = Math.min(...arr2);
//     if ((arr1Max < arr2Max) && (arr1Min > arr2Min)) {
//         return true;
//     } else {
//         return false;
//     }
// }

// canNest([1, 2, 3, 4], [0, 6])

/*<----------------------------------------------------------------------------->*/

// You will be given two extremely similar arrays, but exactly one of
//  the items in an array will be valued slightly higher than its counterpart
//   (which means that evaluating the value > the other value will return true).

// Create a function that returns whether the first array is slightly superior to that of the second.

// function isFirstSuperior(arr1, arr2) {
//     return arr1 > arr2;
// }

// isFirstSuperior(['zebra', 'ostrich', 'whale'], ['ant', 'ostrich', 'whale'])

/*<----------------------------------------------------------------------------->*/

// Create a function that receives a non-negative integer and returns the factorial of that number.

// function fact(n) {
//     let returnNum = 1;
//     if (n == 0) {
//         return 1;
//     } else {
//         for (let i = 1; i <= n; i++) {
//             returnNum = returnNum * i;
//         }
//     }
//     return returnNum;
// }

// fact(7)

/*<----------------------------------------------------------------------------->*/

// Create a function that takes a number as an argument. Add up all the numbers from 1 to the number you
//  passed to the function. For example, if the input is 4 then your function should
//   return 10 because 1 + 2 + 3 + 4 = 10.

// function addUp(num) {
//     let sum = 0;
//     for (let i = 1; i <= num; i++) {
//         sum += i;
//         console.log(sum)
//     }

//     return sum;
// }

// addUp(1000)

/*----------------8/27/20  6:18 AM---------------------*/

// /*
// Create a function that returns an array of strings sorted by length in ascending order.

// function sortByLength(arr) {
//     let newArr = arr.sort((a, b) => {
//         return a.length - b.length;
//     })
//     return newArr;
// }

// sortByLength(["apple", "pie", "shortcake"])

/*<----------------------------------------------------------------------------->*/

// Christmas Eve is almost upon us, so naturally we need to prepare some milk and cookies for Santa!
// Create a function that accepts a Date object and returns true if it's Christmas Eve (December 24th)
// and false otherwise.Keep in mind JavaScript's Date month is 0 based, meaning December is the 11th
// month while January is 0.

// function timeForMilkAndCookies(date) {
//     let xmas = date;
//   if (xmas.getMonth() === 11 && xmas.getDate() == 24) {
//       return true;
//   } else {
//      return false;
//   }
// }

// //timeForMilkAndCookies(new Date(2013, 11, 24))
// timeForMilkAndCookies(new Date(2010, 11, 2))

/*<----------------------------------------------------------------------------->*/

// Create a function that calculates the number of different squares in an n * n square grid.
// Check the Resources tab.

// function numberSquares(n) {
//     let x = n * (n + 1) * (2 * n + 1) / 6
//     return x;
// }

// numberSquares(12)

/*<----------------------------------------------------------------------------->*/

// Create a function that takes two arrays and insert the second array in the middle of the first array.\
// function tuckIn(arr1, arr2) {
//     arr1.splice(1, 0, ...arr2);
//     console.log(arr1)
// }

// //tuckIn([1, 10], [2, 3, 4, 5, 6, 7, 8, 9])
// //tuckIn([15, 150], [45, 75, 35])
// tuckIn(["bottom", "topping"], ["tomatosauce", "vegetables", "cheese"])
// //tuckIn([[1, 2], [5, 6]], [[3, 4]])

/*<----------------------------------------------------------------------------->*/

// Write a function that takes a two-digit number and determines if it's the largest of two possible digit swaps.

// function largestSwap(num) {
//     let largest;
//     let sum = num;
//     let digits = ("" + num).split("").map(Number);
//     digits.reduce((a, b) => {
//         if (a >= b) {
//             largest = true;
//         } else if (a < b) {
//             largest = false
//         }
//     })
//     return largest;
// }
// largestSwap(99)

/*<----------------------------------------------------------------------------->*/

// Create a function that takes an angle in radians and converts it into degrees.

// function toDegree(radian) {
//     return Math.ceil(radian * 180 / Math.PI);
// }
// toDegree(Math.PI / 3)

/*<----------------------------------------------------------------------------->*/

// Create a simple promise and pass the resolve function a string value of your choice. Use the setTimeout
//  function as your asynchronous operation.
//  Your setTimeout() function should not exceed 1000ms. Store the promise inside a variable named promise.

// let promise2 = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         resolve('JavaScript is LIFE!')
//     }, 1000)
// })

// promise2.then((res) => {
//     console.log(res)
// })

/*<----------------------------------------------------------------------------->*/

// Create a function that takes an array and a string as arguments and returns the array of CMSs that include
// the given string. Return the names in an array in alphabetical order.

// function cmsSelector(arr, str) {
//     let cms = arr.filter((a) => {
//         if ((a.toLowerCase().includes(str))) {
//             return a;
//         }
//     }).sort();
//     return cms;
// }

// cmsSelector(["WordPress", "Joomla", "Drupal"], "ru")

/*<----------------------------------------------------------------------------->*/

// Create a function that takes an array of integers and removes the smallest value.

// function removeSmallest(arr) {
//     let min = Math.min(...arr);
//     let index = arr.indexOf(min)
//     arr.splice(index, 1)
//     return arr;

// }
// removeSmallest([5, 3, 2, 1, 4])

/*<----------------------------------------------------------------------------->*/

// I am trying to filter out empty arrays from an array. In other words, I want to
// transform something that looks like this: ["a", "b", [], [], [1, 2, 3]] to look
// like ["a", "b", [1, 2, 3]].

// function removeEmptyArrays(arr) {
//     let a = arr.filter(e => e.length !== 0)
//     return a;
// }

// removeEmptyArrays([1, 2, [], 4])

/*<----------------------------------------------------------------------------->*/

// Implement a function that returns an array containing all the consecutive numbers in
// ascendant order from the given value low up to the given value high (bounds included).

// function getSequence(low, high) {
//     let arr = []
//     for (let i = low; i <= high; i++) {
//         arr.push(i)
//     }
//     return arr;
// }

// getSequence(1000, 1000)

/*<----------------------------------------------------------------------------->*/

// You are given an array with random words but your program doesn't accept words that begin
// with the capital letter "C". Remove the unaccepted words and return the new array.

// function acceptedWords(arr) {
//     let start = arr.filter((a) => {
//         return a.charAt(0) !== 'C'
//     })
//     return start;
// }

// acceptedWords(["cars", "trucks", "planes"])

/*<----------------------------------------------------------------------------->*/

// Write a function redundant that takes in a string str and returns a function that returns str.

// function redundant(str) {
//     function returnStr(str) {
//         return str;
//     }
//    returnStr(str)
// }

// redundant("apple")

/*<----------------------------------------------------------------------------->*/

// Create a function that takes a string of lowercase characters and returns that string
// reversed and in upper case.
//

// function reverseCapitalize(str) {
//     let upper = str.toUpperCase();
//     let arr = Array.from(upper).reverse().join('');
//     return arr;
// }

// reverseCapitalize("abc")

// let promise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         resolve('JS IS THE BEST!');
//         reject('Python can suck a big one! And also this messed up.');
//     }, 1000)
// })

/*<----------------------------------------------------------------------------->*/

// Closures are functions that remember their lexical environments. Lexical
//  environments mean the environment in which the function was declared.

// function greetingMaker(salutation) {
//     return function closure(name) {
//         return salutation + ", " + name;
//     }
// }
// const greeting = greetingMaker("Hello");
// console.log(greeting("James"));

/*----------------8/28/20  3:32 AM---------------------*/

// You will be given an array of drinks, with each drink being an object with two properties:
//  name and price. Create a function that has the drinks array as an argument and return the
//  drinks object sorted by price in ascending order.

//function sortDrinkByPrice(drinks) {
// drinks.sort((a, b) => {
//     if (a.price > b.price) {
//         return 1;
//     } else {
//         return -1;
//     }
// })
// return drinks;

//using for loop
//     for (let i = 0; i < drinks.length; i++) {
//         for (let j = i; j < drinks.length; j++) {
//             if (drinks[i].price > drinks[j].price) {
//                 let temp = drinks[i];
//                 drinks[i] = drinks[j];
//                 drinks[j] = temp;
//             }
//         }
//     }
//    return drinks;
// }
// const drinks1 = [{
//         name: 'lemonade',
//         price: 90
//     },
//     {
//         name: 'lime',
//         price: 432
//     },
//     {
//         name: 'peach',
//         price: 23
//     }
// ];
// sortDrinkByPrice(drinks1)

/*<----------------------------------------------------------------------------->*/

// A palindrome is a word that is identical forward and backwards.

//     mom
//     racecar
//     kayak

// Given a word, create a function that checks whether it is a palindrome.

// function checkPalindrome(str) {
//     let orig = str.toLowerCase();
//     let newStr = str.toLowerCase().split('').reverse().join('');
//     return orig === newStr ? true : false;
// }
// checkPalindrome('mom')
// //checkPalindrome('scary')
// //checkPalindrome('reviver')

/*<----------------------------------------------------------------------------->*/

// Create a function that finds the index of a given item.

// function search(arr, item) {
//     // let found = arr.findIndex((a) => {
//     //     return a === item;

//     // })
//     // console.log(found)

//     // Using loop
//     for (let i = 0; i < arr.length; i++) {
//         if (arr[i] === item) {
//             return console.log(i);
//         }
//     }

// }

// search([1, 5, 3], 5)

/*<----------------------------------------------------------------------------->*/

// Write a function that returns 0 if the input is 1, and returns 1 if the input is 0.

// function flip(y) {
//     return y === 0 ? 1 : 0;
// }
// flip(0)

/*<----------------------------------------------------------------------------->*/

// Create a function that takes an array of non-negative integers and strings and return
// a new array without the strings.

//function filterArray(arr) {
// let filter = arr.filter((a) => {

//     return typeof a === 'number'
// })
// console.log(filter)

//using for loop
// let filter = []
// for (let i = 0; i < arr.length; i++) {
//     if (typeof arr[i] === 'number') {
//         filter.push(arr[i])
//     }
// }
// console.log(filter)
// }

// filterArray([1, 2, "a", "b"])

/*<----------------------------------------------------------------------------->*/

// Take an array of integers (positive or negative or both) and return the sum of the absolute
// value of each element.

// function getAbsSum(arr) {
//     // let sum = 0;
//     // for (let i = 0; i < arr.length; i++) {
//     //     sum += Math.abs(arr[i]);
//     // }
//     // return sum;

//     // using reduce
//     let sum = arr.reduce((pre, cur) => {
//         return Math.abs(pre) + Math.abs(cur);
//     }, )
//     console.log(sum)
// }
// getAbsSum([-1, -3, -5, -4, -10, 0])

/*<----------------------------------------------------------------------------->*/

// Create a function that takes an array as an argument and returns true or false depending on whether
// the average of all elements in the array is a whole number or not.

//function isAvgWhole(arr) {
// es5
// let sum = 0;
// for (let i = 0; i < arr.length; i++) {
//     sum += arr[i];
// }
// let ave = sum / arr.length;
// if (ave % 1 === 0) {
//     return true;
// } else {
//     return false;
// }

//es6
// let sum = arr.reduce((pre, cur) => {
//     return pre + cur;
// });
// let ave = (sum / arr.length)
// return Number.isInteger(ave) ? true : false;
//}

//isAvgWhole([3, 5, 9])

/*<----------------------------------------------------------------------------->*/

// Create a function that takes a string and returns a string in which each character is repeated once.
//function doubleChar(str) {
//es5
//     let arr = []
//     for (let i = 0; i < str.length; i++) {
//         arr.push(str[i], str[i])
//     }
//     let doubleString = arr.join('')
//     return doubleString;

//es6
//     let doubleString = str.split('').map(function (v) {
//         return v + v;
//     }).join('');

//     return doubleString;
// }

// doubleChar("String")

/*<----------------------------------------------------------------------------->*/

// Create a function that takes an integer and returns the factorial of that integer.
// That is, the integer multiplied by all positive lower integers.
//function factorial(int) {
//es5
// var output = 1;
// for (i = 1; i <= int; i++) {
//     output = i * output
// }
// return output

//es6 maybe?..

//     if (int === 1) {
//         return int;
//     } else {
//         return int * factorial(int - 1);
//     }

// }
// factorial(5)

/*<----------------------------------------------------------------------------->*/

// Create a recursive function that takes two parameters and repeats the string
// n number of times. The first parameter txt is the string to be repeated and the
// // second parameter is the number of times the string is to be repeated.
// function repetition(txt, n) {
//     if (n === 1) {
//         return txt;
//     }
//     return txt + repetition(txt, n - 1)
// }

// repetition("soccer", 2);

/*----------------8/29/20  11:42 AM---------------------*/

// Create a function that takes a string and returns the number (count) of vowels contained within it.

//function countVowels(str) {
//es5
// let amt = 0;
// str.toLowerCase();
// for (let i = 0; i < str.length; i++) {
//     console.log(str[i])
//     if (str[i] === 'a' || str[i] === 'e' || str[i] === 'i' || str[i] === 'o' || str[i] === 'u') {
//         amt += 1;
//     }
// }
// return amt;

//es6
//     let vowel = [...str].filter(c => 'aeiou'.includes(c.toLowerCase())).length;
//     return vowel;
// }

// countVowels("Celebration")

/*<----------------------------------------------------------------------------->*/

// Assume a program only reads .js or .jsx files. Write a function that accepts a file path and
// returns true if it can read the file and false if it can't.

// function isJS(path) {
//     //es6
//     // let filePath = path;
//     // let js = /js/i;
//     // let jsx = /jsx/i;
//     // return filePath.match(js) || filePath.match(jsx) ? true : false;

//     //es5
//     let filePath = path;
//     let js = /js/i;
//     let jsx = /jsx/i;
//     if (filePath.match(js) || filePath.match(jsx)) {
//         return true;
//     } else {
//         return false;
//     }

// }
// isJS("/users/user.jsx")
// isJS("/users/user.js")
// isJS("/users/user.ts")
// isJS("/users/user.jpg")
// isJS("/users/user.ext")
// isJS("/users/user.php")

/*<----------------------------------------------------------------------------->*/

// Create a function that takes an array of numbers and returns the second largest number.

// function secondLargest(arr) {

//     //es5

//     for (let i = 0; i < arr.length; i++) {
//         for (let j = i; j < arr.length; j++) {
//             // console.log('I IT:', i, 'J IT:', j)
//             // console.log('I EL:', arr[i], 'J EL:', arr[j])
//             if (arr[i] < arr[j]) {
//                 let temp = arr[i]; // store original value for swapping
//                 arr[i] = arr[j]; // set original value position to greater value
//                 arr[j] = temp; // set greater value position to original value
//                 console.log('TEMP', temp, '1ST LOOP EL:', arr[i], '2ND LOOP EL:', arr[j])
//             }
//         }
//     }
//     return arr[1];
// }

// secondLargest([10, 40, 30, 20, 50])
// secondLargest([25, 143, 89, 13, 105])
// secondLargest([54, 23, 11, 17, 10])
// secondLargest([513, 211, 131, 417, 11])
// secondLargest([63, 44, 11, 22, 33, 66, 65])

/*<----------------------------------------------------------------------------->*/

// Write a function that takes a non-negative integer and return its factorial.

// function factorial(z) {
//     console.log(z)
//     if (z <= 1) {
//         return 1;
//     }
//     return z * factorial(z - 1);
// }

// factorial(4)
// // factorial(0)
// // factorial(9)
// // factorial(1)
// // factorial(2)

/*----------------8/30/20  7:30 AM---------------------*/

// Given an array of numbers, return an array which contains all the even numbers
// in the orginal array, which also have even indices.

//function getOnlyEvens(nums) {
//es5
// let evenArray = []
// for (let i = 0; i < nums.length; i++) {
//     if (nums[i] % 2 === 0 && i % 2 === 0) {
//         evenArray.push(nums[i])
//     }
// }
// return evenArray;

//3s6

//     let evenArray = nums.filter((a, idx) => {
//         return (a % 2 === 0 && idx % 2 === 0)
//     })
//     return evenArray;
// }

// // getOnlyEvens([1, 3, 2, 6, 4, 8])
// // getOnlyEvens([0, 1, 2, 3, 4])
// // getOnlyEvens([1, 2, 3, 4, 5])
// // getOnlyEvens([37, 26, 18, 42, 2, 30])/

/*<----------------------------------------------------------------------------->*/

// Create a function that takes two strings as arguments and returns the number of times the
// first string (the single character) is found in the second string.

//function charCount(myChar, str) {
//es5
// let arrSame = [];
// for (let i = 0; i < str.length; i++) {
//     console.log(str[i])
//     for (let j = 0; j < myChar.length; j++) {
//         if (myChar[j] === str[i]) {
//             arrSame.push(myChar[j])
//         }
//     }

// }
// return arrSame.length;

//es6
//  return ([...str].filter(x => x === myChar).length);
//}

//charCount('a', 'edabit')
//charCount('b', 'big fat bubble')
//charCount('c', 'Chamber of secrets')

/*<----------------------------------------------------------------------------->*/

// Given an array of scrabble tiles, create a function that outputs the maximum possible
// score a player can achieve by summing up the total number of points for all the tiles
// in their hand. Each hand contains 7 scrabble tiles.

// function maximumScore(tileHand) {
//     //es5
//     let tlt = 0;
//     for (let i = 0; i < tileHand.length; i++) {
//         tlt += tileHand[i].score;
//     }
//    return(tlt)

//es6
// let max = tileHand.reduce((pre, cur) => {
//     return pre + cur.score
// }, 0)
// return max;
//}

// maximumScore([{
//     tile: 'N',
//     score: 1
// }, {
//     tile: 'K',
//     score: 5
// }, {
//     tile: 'Z',
//     score: 10
// }, {
//     tile: 'X',
//     score: 8
// }, {
//     tile: 'D',
//     score: 2
// }, {
//     tile: 'A',
//     score: 1
// }, {
//     tile: 'E',
//     score: 1
// }])

/*<----------------------------------------------------------------------------->*/

//Write a function that calculates the factorial of a number recursively.
// function factorial(n) {
//     if (n <= 1) {
//         return 1;
//     }
//     return n * factorial(n-1)
// }

// factorial(5)
// factorial(3)
// factorial(1)
// factorial(0)

/*<----------------------------------------------------------------------------->*/

//Write a function that takes a string as an argument and returns the left most digit in the string.

//function leftDigit(num) {
//es5
// let numArr = [];
// for (let i = 0; i < num.length; i++) {
//     if (num[i] === '1' || num[i] === '2' || num[i] === '3' ||
//         num[i] === '4' || num[i] === '5' || num[i] === '6' ||
//         num[i] === '7' || num[i] === '8' ||
//         num[i] === '9'
//     ) {
//         numArr.push(parseInt(num[i]))
//     }
// }
// console.log(numArr[0])

// regular expression
//     let numbers = /[0-9]/;
//     let returnNum = parseInt((num.match(numbers)));
//     return returnNum;
// }

// leftDigit("TrAdE2W1n95!")
// leftDigit("V3r1ta$")
// leftDigit("U//DertHe1nflu3nC3")
// leftDigit("J@v@5cR1PT")
// leftDigit("0nSlaUgh7*d3atH")
// leftDigit("F8andD3st1nY")

/*<----------------------------------------------------------------------------->*/

// Create a function that finds the word "bomb" in the given string.
//  If found, return "Duck!!!", otherwise return "There is no bomb, relax.".

//function bomb(str) {
//es5
// let isDanger = str.slice(0, -1).split(' ');
// let danger = "There is no bomb, relax.";
// for (let i = 0; i < isDanger.length; i++) {
//     if (isDanger[i].toLowerCase() === "bomb") {
//         danger = "Duck!!!"
//     }
// }
// return danger;

// es6
//return str.toLowerCase().includes('bomb') ? "Duck!!!" : "There is no bomb, relax.";
//}

// bomb("There is a bomb.") //, "Duck!!!")
// bomb("Hey, did you find it?") //, "There is no bomb, relax.")
// bomb("Hey, did you think ther is a bomb?") //, "Duck!!!")
// bomb("This goes boom!!!") //, "There is no bomb, relax.")
// bomb("Hey, did you find the BoMb?") //, "Duck!!!")

/*<----------------------------------------------------------------------------->*/

//Given an array and an integer n, return the sum of the first n numbers in the array.

// function sliceSum(arr, n) {
//     //es5
//     let sum = 0;
//     if (n > arr.length) {
//         for (let i = 0; i < arr.length; i++) {
//             sum += arr[i]
//         }
//     } else {
//         for (let i = 0; i < n; i++) {
//             sum += arr[i]
//         }
//     }
//     return sum

//     //es6
//     return arr.slice(0,n).reduce((acc,curr) => acc + curr, 0);
// }
// sliceSum([1, 3, 2, 6], 3)

/*----------------8/31/20  1:46 PM ---------------------*/

// // Create a function that takes an array of numbers and returns a new array,
// // sorted in ascending order (smallest to biggest).

// //     Sort numbers array in ascending order.
// //     If the function's argument is null, an empty array, or undefined; return an empty array.
// //     Return a new array of sorted numbers.

// function sortNumsAscending(arr) {
//     //es5
//     // if (arr === [] || arr === null) {
//     //     return []
//     // }
//     // for (let i = 0; i < arr.length; i++) {
//     //     for (let j = i; j < arr.length; j++) {
//     //         if (arr[i] > arr[j]) {
//     //             let temp = arr[j];
//     //             arr[j] = arr[i]
//     //             arr[i] = temp;
//     //         }
//     //     }
//     // }
//     // return arr;

//     //es6
//     if (arr === [] || arr === null) {
//         return []
//     }
//     arr.sort((a, b) => a - b)
//     return arr;
// }

// sortNumsAscending([1, 2, 10, 50, 5])
// sortNumsAscending([80, 29, 4, -95, -24, 85])
// sortNumsAscending(null)
// sortNumsAscending([])
// sortNumsAscending([47, 51, -17, -16, 91, 47, -85, -8, -16, -27])
// sortNumsAscending([-51, -73, 65, 69, -76, 74, -14])
// sortNumsAscending([45, 98, 35, 65, 97, 21, 33])
// sortNumsAscending([-23, -69, -54, -2, -32])
// sortNumsAscending([-21, -9, -96])
// sortNumsAscending([0])

/*<----------------------------------------------------------------------------->*/

// Create a function that takes in an array of numbers and returns the sum of its cubes.
// function sumOfCubes(nums) {
//     //es5
//     // let cube = 0
//     // for (let i = 0; i < nums.length; i++) {
//     //     cube += Math.pow(nums[i], 3);
//     // }
//     // return cube;

//     //es6
//     return nums.reduce((p, c) => p + Math.pow(c, 3), 0);
// }

// sumOfCubes([1, 5, 9])

/*<----------------------------------------------------------------------------->*/

// Write a function that takes a string name and a number num (either 0 or 1) and return
// "Hello" + name if num is 1, otherwise return "Bye" + name.
// function sayHelloBye(name, num) {
//     const fname = name.charAt(0).toUpperCase() + name.slice(1)
//     if (num === 1) {
//         return `Hello ${fname}`;
//     } else {
//         return `Bye ${fname}`;
//     }
// }
// sayHelloBye("jose", 1)
// sayHelloBye("jose", 1)

/*----------------9/01/20  5:05 AM ---------------------*/

//Create a function that takes a string and returns a new string with all vowels removed.

// function removeVowels(str) {
//    return str.replace(/[aeiou]/gi, '');

// }

//removeVowels("If Obama resigns from office NOW, thereby doing a great service to the country—I will give him free lifetime golf at any one of my courses!")
// removeVowels("This election is a total sham and a travesty. We are not a democracy!")
// removeVowels("I have never seen a thin person drinking Diet Coke.")
// removeVowels("Everybody wants me to talk about Robert Pattinson and not Brian Williams—I guess people just don’t care about Brian!")
// removeVowels("Katy, what the hell were you thinking when you married loser Russell Brand. There is a guy who has got nothing going, a waste!")
// removeVowels("Windmills are the greatest threat in the US to both bald and golden eagles. Media claims fictional ‘global warming’ is worse.")
// removeVowels("Sorry losers and haters, but my I.Q. is one of the highest -and you all know it! Please don’t feel so stupid or insecure,it’s not your fault")
// removeVowels("Happy Thanksgiving to all--even the haters and losers!")
// removeVowels("Watch Kasich squirm --- if he is not truthful in his negative ads I will sue him just for fun!")
// removeVowels("Obama is, without question, the WORST EVER president. I predict he will now do something really bad and totally stupid to show manhood!")

/*<----------------------------------------------------------------------------->*/

// Create a function that takes an array of 10 numbers
// (between 0 and 9) and returns a string of those numbers formatted as a phone number (e.g. (555) 555-5555).

// function formatPhoneNumber(numbers) {
//     let first = numbers.splice(0, 3).join('')
//     let second = numbers.splice(0, 3).join('')
//     let last = numbers.splice(0, 4).join('')
//     let phoneNum = `(${first}) ${second}-${last}`
//     return phoneNum
// }

// formatPhoneNumber([1, 2, 3, 4, 5, 6, 7, 8, 9, 0])

/*<----------------------------------------------------------------------------->*/

//Create a function that returns the number of hashes and pluses in a string.
// function hashPlusCount(str) {
//     let arr = str.split('')
//     let newArr = []
//     let plusSigns = 0;
//     let poundSigns = 0;
//     for (let i = 0; i < arr.length; i++) {
//         if (arr[i] == '+') {
//             plusSigns++
//         } else if (arr[i] == '#') {
//             poundSigns++;
//         }
//     }
//     newArr[0] = poundSigns;
//     newArr[1] = plusSigns;
//     return newArr
// }

// hashPlusCount("#+#+")
// hashPlusCount("####")
// hashPlusCount("+++++++")

/*<----------------------------------------------------------------------------->*/

// Create a function that takes any nonnegative number as an argument and return it with it's digits in
// descending order. Descending order is when you sort from highest to lowest.

// function sortDescending(num) {
//     //es5
//     let arr = num.toString().split("")
//     console.log(arr)
//     for (let i = 0; i < arr.length; i++) {
//         for (let j = 0; j < arr.length; j++) {
//             if (arr[i] > arr[j]) {
//                 let temp;
//                 temp = arr[j];
//                 arr[j] = arr[i];
//                 arr[i] = temp;
//             }
//         }
//     }
//     let returnNums = parseInt(arr.join(""));
//     return returnNums;
// }

// sortDescending(45123)

/*<----------------------------------------------------------------------------->*/

//Create a function that takes an array of arrays with numbers. Return a new (single)
//array with the largest numbers of each.

//function findLargestNums(arr) {
//es5
// var result = [];
// for (i = 0; i < arr.length; i++) {
//     result.push(Math.max.apply(null, arr[i]));
// };
// return result;

// //es6
// return arr.map(x => Math.max(...x));
//}

// findLargestNums([
//     [4, 2, 7, 1],
//     [20, 70, 40, 90],
//     [1, 2, 0]
// ])

// findLargestNums([
//     [0.4321, 0.7634, 0.652],
//     [1.324, 9.32, 2.5423, 6.4314],
//     [9, 3, 6, 3]
// ])

// findLargestNums([
//     [-34, -54, -74],
//     [-32, -2, -65],
//     [-54, 7, -43]
// ]) //, [-34, -2, 7])
// findLargestNums([
//     [0.34, -5, 1.34],
//     [-6.432, -1.762, -1.99],
//     [32, 65, -6]
// ]) //, [1.34, -1.762, 65])
// findLargestNums([
//     [0, 0, 0, 0],
//     [3, 3, 3, 3],
//     [-2, -2]
// ]) //, [ 0, 3, -2 ])

/*----------------9/03/20  5:36 AM ---------------------*/

//Create a function that sorts an array and removes all duplicate items from it.

// function set(arr) {
//     //es5
//     // const finalArr = [];
//     // let current = arr[0];
//     // finalArr.push(current);

//     // for (let i = 1; i < arr.length; i++) {
//     //     if (arr[i] !== current) {
//     //         current = arr[i];
//     //         finalArr.push(current);
//     //     }
//     // }
//     // return finalArr;

//     //es6
//     let newArr = arr.filter((a, i, self) => {
//         // console.log(a)
//         // console.log(i)
//         // console.log(self)
//         return i == self.indexOf(a);
//     })
//    return newArr
// }

// set([1, 3, 3, 5, 5])
// set([4, 4, 4, 4])
// set([5, 7, 8, 9, 10, 15])

/*<----------------------------------------------------------------------------->*/

//Create a function that takes in an array of names and returns the name of the secret society.

//function societyName(friends) {
//es5
//     let newArr = []
//     let tmp;
//     for (let i = 0; i < friends.length; i++) {
//         for (let j = i; j < friends.length; j++) {
//             if (friends[i] > friends[j]) {
//                 tmp = friends[i];
//                 friends[i] = friends[j];
//                 friends[j] = tmp;
//             }
//         }
//         newArr.push(friends[i].charAt(0))
//     }
//   return newArr.join('');

//es6
//     let newArr = friends.map((a) => {
//         return a.charAt(0);
//     })
//     return newArr.sort().join('');
// }

// societyName(['Adam', 'Sarah', 'Malcolm'])
// societyName(['Phoebe', 'Chandler', 'Rachel', 'Ross', 'Monica', 'Joey'])

/*<----------------------------------------------------------------------------->*/

//Create a function that takes an array of numbers and returns the mean value.

// function mean(arr) {
//     //es5
//     // let sum = 0;
//     // for (let i = 0; i < arr.length; i++) {
//     //     sum += arr[i]

//     // }
//     // let mean = sum / arr.length
//     // return +mean.toFixed(2)

//     //es6
//     let newArr = arr.reduce((p, c) => {
//         return p + c;
//     })
//     newArr = newArr / arr.length;
//     return +newArr.toFixed(2);

// }

// mean([1, 0, 4, 5, 2, 4, 1, 2, 3, 3, 3]) //2.55

/*<----------------------------------------------------------------------------->*/

// Create a function that returns the index of the first vowel in a string.

//function firstVowel(str) {
//es5
// let newStr = str.toLowerCase();
// for (let i = 0; i < newStr.length; i++) {
//     if (newStr[i] === 'a' ||
//         newStr[i] === 'e' ||
//         newStr[i] === 'i' ||
//         newStr[i] === 'o' ||
//         newStr[i] === 'u') {
//         return i;
//     }
// }

//es6
// return str.search(/a|e|i|o|u/i))
//}

//firstVowel("STRAWBERRY")

/*<----------------------------------------------------------------------------->*/

// Given a number n, write a function that returns PI to n decimal places.
// function myPi(n) {
//     return +Math.PI.toFixed(n);
// }
// myPi(0)
// myPi(5)

/*<----------------------------------------------------------------------------->*/

//Given an array of 10 numbers, return the maximum possible total made by summing just 5 of the 10 numbers.

// function maxTotal(nums) {
//     let sortmax = nums.sort((a,b)=>b-a);
// 	let total =0;
// 	//return sortmax[0]+sortmax[1]+sortmax[2]+sortmax[3]+sortmax[4];
// 	for(let i =0; i<5; i++){
// 		total +=sortmax[i];
// 	}
// 	return total;
// }

// maxTotal([1, 1, 0, 1, 3, 10, 10, 10, 10, 1])//, 43)

/*<----------------------------------------------------------------------------->*/

//Create a function that determines whether an input value is omnipresent for a given array.

// function isOmnipresent(arr, val) {
//     //es5
//     let newArr = arr.every(x => x.includes(val))
//     console.log(newArr)
// }

// isOmnipresent([
//     [1, 1],
//     [1, 3],
//     [5, 1],
//     [6, 1]
// ], 1) //, 1), true)

// Write a regular expression that matches a string
// if and only
// if it is a valid zip code.
//let x = /(^\d{5}$)|(^\d{5}-\d{4}$)/

/*<----------------------------------------------------------------------------->*/

// Create a function that returns true if the first array is a subset of the second. Return false otherwise.

// function isSubset(arr1, arr2) {
//     let newArr = arr1.every((x) => {
//         return arr2.includes(x)
//     })
//     return newArr;
// }

// isSubset([3, 2, 5], [5, 3, 7, 9, 2]) //, true)
// isSubset([1, 2], [3, 5, 9, 1]) //, false

/*<----------------------------------------------------------------------------->*/

//Create a function which returns the number of true values there are in an array.

//function countTrue(arr) {
//es5
// let trueAmt = 0;
// for (let i = 0; i < arr.length; i++) {
//     if (arr[i] === true) {
//         trueAmt++
//     }
// }
// console.log(trueAmt)
//es6
//     let trueAmt = arr.filter((a) => {
//         return a === true ? a : 0
//     })
//     return trueAmt.length;
// }

// countTrue([true, false, false, true, false]) //, 2)
// countTrue([false, false, false, false]) //, 0)

/*<----------------------------------------------------------------------------->*/

// Given an object of how many more pages each ink color
// can print, output the maximum number of pages the
// printer can print before any of the colors run out.

//function inkLevels(inks) {
//es5
// let inkLevel = 0;
// for (let i in inks) {
//     for (let j in inks) {
//         if (inks[j] < inks[i] || inks[j] === inks[i]) {
//             let temp;
//             temp = inks[j];
//             inks[j] = inks[i];
//             inks[i] = temp;
//             inkLevel = inks[i]

//         }

//     }
// }
// return inkLevel;

//es6

//}

// inkLevels({
//     "cyan": 23,
//     "magenta": 12,
//     "yellow": 10
// }) //, 10);

// inkLevels({
//     "cyan": 432,
//     "magenta": 543,
//     "yellow": 777
// }) //, 432);

// inkLevels({
//     "cyan": 700,
//     "magenta": 700,
//     "yellow": 0
// }) //, 0);

// inkLevels({
//     "cyan": 700,
//     "magenta": 700,
//     "yellow": 700
// }) //, 700);

// inkLevels({
//     "cyan": 1,
//     "magenta": 1,
//     "yellow": 1
// }) //, 1);

/*<----------------------------------------------------------------------------->*/

//Given a string, create a function to reverse the case.
// All lower-cased letters should be upper-cased, and vice versa.

//function reverseCase(str) {
//es5
// let newString = '';
// for (let i = 0; i < str.length; i++) {
//     let letter = str.charAt(i);
//     if (letter == letter.toUpperCase()) {
//         letter = letter.toLowerCase();
//     } else {
//         letter = letter.toUpperCase();
//     }
//     newString += letter;
// }
// return newString;

//}

//reverseCase('Happy Birthday') //, 'hAPPY bIRTHDAY')

/*----------------9/04/20  3:59 AM ---------------------*/

//Create a function to count the number of 1s in a 2D array.

//function countOnes(matrix) {
//es5
// let oneCount = 0;
// for (let i = 0; i < matrix.length; i++) {
//     for (let j = 0; j < matrix[i].length; j++) {
//         if (matrix[i][j] === 1) {
//             oneCount += 1;
//         }
//     }
// }
// return oneCount;

//es6
// return matrix.toString().split(',').filter(x => x == '1').length
//}

// countOnes([
//     [1, 0, 1],
//     [0, 0, 0],
//     [0, 0, 1]
// ]) //, 3)

// countOnes([
//     [1, 1, 1],
//     [0, 0, 1],
//     [1, 1, 1]
// ]) //, 7)

// countOnes([
//     [1, 2, 3],
//     [0, 2, 1],
//     [5, 7, 33]
// ]) //, 2)

// countOnes([
//     [5, 2, 3],
//     [0, 2, 5],
//     [5, 7, 33]
// ]) //, 0)

// countOnes([
//     [5, 2],
//     [0, 2],
//     [5, 1]
// ]) //, 1)

// countOnes([
//     [1, 1],
//     [0, 1]
// ]) //, 3)

// countOnes([
//     [0, 1],
//     [0, 0]
// ]) //, 1)

/*<----------------------------------------------------------------------------->*/

//Create a function that will return an integer number containing the amount
//of digits in the given integer num.

//function num_of_digits(num) {
//es5
// let arr = num.toString().split('')
// for (let i = 0; i < arr.length; i++) {
//     if (arr[i] === '-') {
//         arr.splice(i, 1)
//     }
// }
// let numLength = arr.length;
// return numLength;

//es6
//return Math.abs(num).toString().split('').length;
//}

// num_of_digits(13124) //, 5)
// num_of_digits(0) //, 1)
// num_of_digits(-12381428) //, 8)
// num_of_digits(12) //, 2)
// num_of_digits(42) //, 2)
// num_of_digits(1000) //, 4)
// num_of_digits(136) //, 3)
// num_of_digits(1000000000) //, 10)
// num_of_digits(2147483647) //, 10)
// num_of_digits(-2147483647) //, 10)

/*<----------------------------------------------------------------------------->*/

//Given a string of numbers separated by a comma and space, return the total of all the numbers.

//function addNums(nums) {
//es5
// let arr = nums.split(', ');
// let newArr = [];
// let sum = 0;
// for (let i = 0; i < arr.length; i++) {
//     newArr.push(parseInt(arr[i]))
// }
// for (let i = 0; i < newArr.length; i++) {
//     sum += newArr[i];
// }
// return sum;

//es6
//     let tlt = nums.split(', ').map(a => +a).reduce((p, c) => {
//         return p + c;
//     })
//     return tlt;
// }

// addNums("2, 5, 1, 8, 4") //, 20)
// addNums("2, 5, 1, 8, 4") //, 20)
// addNums("1, 2, 3, 4, 5, 6, 7") //, 28)
// addNums("10") //, 10)
// addNums("-12, -8, 2, 11, -16, 16") //, -7)
// addNums("25, -4, -15, -7, 27, 12, 29, -6, 20, 9") //, 90)

/*<----------------------------------------------------------------------------->*/

// Write two functions:
//     toArray(), which converts a number to an array of its digits.
//     toNumber(), which converts an array of digits back to its number.

// function toArray(num) {
//     let arr = num.toString().split("");
//     let digitArr = [];
//     for (let i = 0; i < arr.length; i++) {
//         digitArr.push(+arr[i])
//     }
//     return digitArr;
// }

// function toNumber(arr) {
//     let str = arr.join('');
//     let digits = +str;
//     return digits;
// }

// toArray(235) //, [2, 3, 5])
// toArray(19) //, [1, 9])
// toArray(0) //, [0] )
// toNumber([2, 3, 5]) //, 235)
// toNumber([1, 9]) //, 19)
// toNumber([0]) //, 0)

/*<----------------------------------------------------------------------------->*/

// Create a function that takes a string.
// If the string is all uppercase characters convert the string to lowercase
// and add an exclamation mark at the end of the string.

// function normalize(str) {
//     //es5
//     let sentence = "";
//     for (let i = 0; i < str.length; i++) {
//         if (str[i] !== ' ' && str[i] !== '.') {
//             if (str[i] !== str[i].toUpperCase()) {
//                 return;
//             } else {
//                 let temp = str.toLowerCase()
//                 let temp2 = temp.replace(temp[0], temp[0].toUpperCase())
//                 sentence = `${temp2}!`
//             }
//         }
//     }
//     return sentence;
// }

// console.log(normalize("CAPS LOCK DAY IS OVER")) //, "Caps lock day is over!", "\n convert the sentence to lowercase and add an exclamation mark(!)\n The first character should be an uppercase C.\n")
// normalize("Today is not October 22th.") //, "Today is not October 22th.", "\n This sentence is correct, no need to modify it.\n")
// normalize("WE WANT THIS COVID THING TO BE OVER") //, "We want this covid thing to be over!")
// normalize("Let us stay calm, no need to panic.") //, "Let us stay calm, no need to panic.")
// normalize("DO NOT SHOUT") //, "Do not shout!")
// normalize("Civilized conversation.") //, "Civilized conversation.")

/*<----------------------------------------------------------------------------->*/

// Create two functions: isPrefix(word, prefix-) and isSuffix(word, -suffix).
//     isPrefix should return true if it begins with the prefix argument.
//     isSuffix should return true if it ends with the suffix argument.
// Otherwise return false.
// function isPrefix(word, prefix) {
//     return word.startsWith(prefix.replace("-", ""));
// }

// function isSuffix(word, suffix) {
//     return word.endsWith(suffix.replace("-", ""));
// }

// isPrefix("automation", "auto-") //, true)
// isPrefix("superfluous", "super-") //, true)
// isPrefix("oration", "mega-") //, false)
// isSuffix("arachnophobia", "-phobia") //, true)
// isSuffix("rhinoplasty", "-plasty") //, true)
// isSuffix("movement", "-scope") //, false)
// isSuffix("vocation", "-logy") //, false)

/*<----------------------------------------------------------------------------->*/

//Create a function that takes an array of strings and return an array, sorted from shortest to longest.
//function sortByLength(arr) {
//es5
//     for (let i = 0; i < arr.length; i++) {
//         for (let j = 0; j < arr.length; j++) {
//             if (arr[i].length < arr[j].length) {
//                 let temp;
//                 temp = arr[j];
//                 arr[j] = arr[i];
//                 arr[i] = temp;
//             }
//         }
//     }
//    return arr;

//es6
//     let sortArr = arr.sort((a, b) => {
//         return a.length - b.length;
//     })
//     return sortArr;
// }

// sortByLength(["Apple", "Google", "Microsoft"]) //, ["Apple", "Google", "Microsoft"])

/*<----------------------------------------------------------------------------->*/

// Write a function that takes all even-indexed characters and odd-indexed characters from
// a string and concatenates them together.

//function indexShuffle(str) {
// //es5
// let evenString = '';
// let oddString = '';
// let fullString = ''
// for (let i = 0; i < str.length; i++) {
//     if (i % 2 === 0) {
//         evenString += str[i]
//     } else {
//         oddString += str[i]
//     }
// }
// fullString = `${evenString}${oddString}`;
// return fullString;

//es6
// const even = [...str].filter((char, i) => i % 2 === 0);
// const odd = [...str].filter((char, i) => i % 2);
// return [...even, ...odd].join('');
//}

// indexShuffle("abcdef") //, "acebdf")
// indexShuffle("abababab") //, "aaaabbbb")
// indexShuffle("it was a beautiful day") //, "i a  euiu atwsabatfldy")
// indexShuffle("maybe") //, "myeab")
// indexShuffle("holiday") //, "hldyoia")

/*<----------------------------------------------------------------------------->*/

// Zip codes consist of 5 consecutive digits. Given a string, write a function
// to determine whether the input is a valid zip code. A valid zip code is as follows:

// Must only contain numbers (no non-digits allowed).
// Must not contain any spaces.
// Must not be greater than 5 digits in length.

// function isValid(zip) {
//     let x = /(^\d{5}$)|(^\d{5}-\d{4}$)/
//     let str = zip;
//     if (str.match(x)) {
//         return true;
//     } else {
//         return false
//     }
// }
// console.log(isValid("59001")) //, true)
// console.log(isValid("853a7")) //, false, "No non-digits allowed.")
// console.log(isValid("732 32")) //, false, "No spaces allowed.")
// console.log(isValid("788876")) //, false, "No sequences of length greater than 5.")
// console.log(isValid("a923b")) //, false, "No letters allowed.")
// console.log(isValid("5923!")) //, false, "No non-digits allowed.")
// console.log(isValid("59238aa")) //, false, "No letters and no sequences of length greater than 5.")
// console.log(isValid("88231")) //, true)

/*<----------------------------------------------------------------------------->*/

// Write a function that returns the number of users in a chatroom based on the following rules:
//     If there is no one, return "no one online".
//     If there is 1 person, return "user1 online".
//     If there are 2 people, return "user1 and user2 online".
//     If there are n>2 people, return the first two names and add "and n-2 more online".

// function chatroomStatus(users) {
//     if (users.length === 0) {
//         return 'no one online'
//     }
//     if (users.length === 1) {
//         return `${users[0]} online`
//     }
//     if (users.length === 2) {
//         return `${users[0]} and ${users[1]} online`
//     }
//     if (users.length > 2) {
//         return `${users[0]}, ${users[1]} and ${users.length - 2} more online`
//     }
// }

// console.log(chatroomStatus([])) //, "no one online")
// console.log(chatroomStatus(["becky325"])) //, "becky325 online")
// console.log(chatroomStatus(["becky325", "malcolm888"])) //, "becky325 and malcolm888 online")
// console.log(chatroomStatus(["becky325", "malcolm888", "fah32fa"])) //, "becky325, malcolm888 and 1 more online")
// console.log(chatroomStatus(["paRIE_to"])) //, "paRIE_to online")
// console.log(chatroomStatus(["s234f", "mailbox2"])) //, "s234f and mailbox2 online")
// console.log(chatroomStatus(["pap_ier44", "townieBOY", "panda321",
//     "motor_bike5", "sandwichmaker833", "violinist91"
// ])) //, "pap_ier44, townieBOY and 4 more online")

/*<----------------------------------------------------------------------------->*/

//Given a word, write a function that returns the first index and the last index of a character.

//function charIndex(word, char) {
//es5
// let returnArr = []
// let firstIndex = word.indexOf(char)
// let lastIndex = word.lastIndexOf(char)
// if (firstIndex === -1) {
//     return undefined;
// }
// returnArr.push(firstIndex, lastIndex)
// return returnArr;

//es6
//return word.match(char) ? [word.indexOf(char), word.lastIndexOf(char)] : undefined;

// }

// charIndex('hello', 'l') //, [2, 3])
// charIndex('circumlocution', 'r') //, [2, 2])
// charIndex('circumlocution', 'i') //, [1, 11])
// charIndex('circumlocution', 'c') //, [0, 8])
// charIndex('happy', 'h') //, [0, 0])
// charIndex('happy', 'p') //, [2, 3])
// console.log(charIndex('happy', 'e')) //, undefined)

/*<----------------------------------------------------------------------------->*/

//Create a function that returns the number of decimal places a number (given as a string)
//has. Any zeros after the decimal point count towards the number of decimal places.

//function getDecimalPlaces(num) {
//     let decIdx = num.indexOf('.')
//     let subStr = num.slice(decIdx)
//     if (decIdx === -1) {
//         return 0;
//     }
//     let returnNum = parseInt(subStr.length - 1)
//     return returnNum
// }

// console.log(getDecimalPlaces("3.22")) //, 2)
// console.log(getDecimalPlaces("400")) //, 0)
// console.log(getDecimalPlaces("43.50")) //, 2)
// console.log(getDecimalPlaces("100,000,000")) //, 0)
// console.log(getDecimalPlaces("3.1415")) //, 4)//
// console.log(getDecimalPlaces("0")) //, 0)//
// console.log(getDecimalPlaces("01")) //, 0)//
// console.log(getDecimalPlaces("00010.00010")) //, 5)//
// console.log(getDecimalPlaces("3,141.592")) //, 3)

/*<----------------------------------------------------------------------------->*/

// Create a function that takes a string and returns
// dashes on the left and right side of every vowel (a e i o u).

// function dashed(str) {
//     let newStr = str.replace(/[aeiou]/gi, '-$&-');
//     return newStr
// }

// console.log(dashed("Edabit")) //, "-E-d-a-b-i-t")
//dashed("Carpe Diem") //, "C-a-rp-e- D-i--e-m")
//dashed("Fight for your right to party!") //, "F-i-ght f-o-r y-o--u-r r-i-ght t-o- p-a-rty!")
//dashed("Finishing off someone’s sentence is annoying, even if you have guessed correctly. Add to that rude, if they stutter.")
//, "F-i-n-i-sh-i-ng -o-ff s-o-m-e--o-n-e-’s s-e-nt-e-nc-e- -i-s -a-nn-o-y-i-ng, -e-v-e-n -i-f y-o--u- h-a-v-e- g-u--e-ss-e-d c-o-rr-e-ctly. -A-dd t-o- th-a-t r-u-d-e-, -i-f th-e-y st-u-tt-e-r.")
//dashed("Fear the soldier who stammers, for he is very fast at pulling the triger.")
//, "F-e--a-r th-e- s-o-ld-i--e-r wh-o- st-a-mm-e-rs, f-o-r h-e- -i-s v-e-ry f-a-st -a-t p-u-ll-i-ng th-e- tr-i-g-e-r.")
//dashed("Thank you, I said bravely, dropping the syllables cleanly, like marbles, and secretly full of the most pathetic pride imaginable.")
//, "Th-a-nk y-o--u-, -I- s-a--i-d br-a-v-e-ly, dr-o-pp-i-ng th-e- syll-a-bl-e-s cl-e--a-nly, l-i-k-e- m-a-rbl-e-s, -a-nd s-e-cr-e-tly f-u-ll -o-f th-e- m-o-st p-a-th-e-t-i-c pr-i-d-e- -i-m-a-g-i-n-a-bl-e-.")

/*<----------------------------------------------------------------------------->*/

// A repdigit is a positive number composed out of the same digit.
// Create a function that takes an integer and returns whether it's a repdigit or not.

//function isRepdigit(num) {
// //     if (num === 0) {
// //         return true;
// //     }
// //     let arr = num.toString().split("")
// //     for (let i = 0; i < arr.length - 1; i++) {
// //         if (arr[i] !== arr[i + 1]) {
// //             return false;
// //         }
// //     }
// //     return true;

// es6
//return String(num).split('').every((x, i, arr) => x=== arr[0])
//}

// console.log(isRepdigit(6)) // , true)
// console.log(isRepdigit(66)) //, true)
// console.log(isRepdigit(666)) //, true)
// console.log(isRepdigit(6666)) //, true)
// console.log(isRepdigit(1001)) //, false)
// console.log(isRepdigit(-11)) //, false, "The number must be >= 0")

/*----------------9/05/20  8:28 AM ---------------------*/

// Create a function that takes three parameters and returns an array with the first parameter x, the
// second parameter y, and every number in between the first and second parameter in ascending
// order. Then filter through the array and return the array with numbers that are only divisible by
// the third parameter n.

//function arrayOperation(x, y, n) {
//es5
// let arr = []
// let divArr = [];
// arr.push(x, y)
// for (let i = x + 1; i < y; i++) {
//     arr.push(i)
// }
// for (let i = 0; i < arr.length; i++) {
//     for (let j = 0; j < arr.length; j++) {
//         if (arr[i] < arr[j]) {
//             let temp;
//             temp = arr[i];
//             arr[i] = arr[j];
//             arr[j] = temp;
//         }
//     }
// }
// for (let i = 0; i < arr.length; i++) {
//     if (arr[i] % n === 0) {
//         divArr.push(arr[i]);
//     }
// }
// return divArr;

//es6
// 	let arr =[]
// for(let i=x;i<=y;i++){
// 	arr.push(i)
// }
// 	return arr.filter(x=>x%n===0)
// }

// arrayOperation(1, 10, 3) //, [3, 6, 9])
// arrayOperation(7, 9, 2) //, [8])
// arrayOperation(15, 20, 7) //, [])
// arrayOperation(10, 50, 10) //, [10, 20, 30, 40, 50])
// arrayOperation(1, 10, 2) //, [2, 4, 6, 8, 10])
// arrayOperation(1, 100, 17) //, [17, 34, 51, 68, 85])
// arrayOperation(15, 20, 5) //, [15, 20])

/*<----------------------------------------------------------------------------->*/

//Write a function that receives two portions of a path and joins them. The portions will be joined
// with the "/" separator. There could be only one separator and if it is not present it should be added.

// function joinPath(portion1, portion2) {
//     let str = '';
//     if (portion1.includes('/') && !portion2.includes('/')) {
//         str = portion1 + portion2;
//     }
//     if (!portion1.includes('/') && portion2.includes('/')) {
//         str = portion1 + portion2;
//     }
//     if (!portion1.includes('/') && !portion2.includes('/')) {
//         str = portion1 + '/' + portion2;
//     }
//     if (portion1.includes('/') && portion2.includes('/')) {
//         let newSub = portion1.replace("/", '')
//         str = newSub + portion2
//     }
//     return str;

//Much better approach
//return `${portion1.replace('/','')}/${portion2.replace('/','')}`;
// }
// joinPath("portion1", "portion2") //, "portion1/portion2")
// joinPath("portion1/", "portion2") //, "portion1/portion2")
// joinPath("portion1", "/portion2") //, "portion1/portion2")
// joinPath("portion1/", "/portion2") //, "portion1/portion2")
// joinPath("5wf7fny", "stJKXlc8") //, "5wf7fny/stJKXlc8")
// joinPath("5wf7fny/", "stJKXlc8") //, "5wf7fny/stJKXlc8")
// joinPath("5wf7fny", "/stJKXlc8") //, "5wf7fny/stJKXlc8")

/*<----------------------------------------------------------------------------->*/

// Steve and Maurice have racing snails. They each have 3,
// a slow (s), medium (m) and fast (f) one. Although Steve's
// snails are all a bit stronger than Maurice's, Maurice has a trick up his sleeve. His plan is:
// Round 1: [s, f] Sacrifice his slowest snail against Steve's fastest.
// Round 2: [m, s] Use his middle snail against Steve's slowest.
// Round 3: [f, m] Use his fastest snail against Steve's middle.
// Create a function that determines whether Maurice's plan will work by
// outputting true if Maurice wins 2/3 games.

//function mauriceWins(mSnails, sSnails) {
// mScore = 0
// sScore = 0
// if (mSnails[0] <= sSnails[2]) {
//     sScore += 1;
// } else {
//     mScore += 1;
// }
// if (mSnails[1] <= sSnails[0]) {
//     sScore += 1;
// } else {
//     mScore += 1;
// }
// if (mSnails[2] <= sSnails[1]) {
//     sScore += 1;
// } else {
//     mScore += 1;
// }

// if (mScore >= 2) {
//     return true
// } else {
//     return false;
// }

// Much better approach
//return mSnails[1] > sSnails[0] && mSnails[2] > sSnails[1]

//}

// console.log(mauriceWins([3, 5, 10], [4, 7, 11])) //, true);
// console.log(mauriceWins([6, 8, 9], [7, 12, 14])) //, false);
// console.log(mauriceWins([1, 8, 20], [2, 9, 100])) //, true);
// console.log(mauriceWins([1, 2, 3], [2, 3, 4])) //, false);
// console.log(mauriceWins([2, 4, 10], [3, 9, 11])) //, true);
// console.log(mauriceWins([3, 8, 13], [5, 11, 15])) //, true);

/*<----------------------------------------------------------------------------->*/

//Write a function that converts an object into an array, where each element represents a key-value pair.
//function toArray(obj) {
//es6
// let arr = Object.entries(obj)
// return arr;

//es5
// var a = [];
// for (var i in obj) {
//     a.push([i, obj[i]]);
// }
// return a;
//}

// console.log(toArray({
//     a: 1,
//     b: 2
// })) //, [["a", 1]//, ["b", 2]])
// console.log(toArray({
//     foo: 33,
//     bar: 45,
//     baz: 67
// })) //, [["foo", 33], ["bar", 45], ["baz", 67]])
// console.log(toArray({
//     shrimp: 15,
//     tots: 12
// })) //, [["shrimp", 15], ["tots", 12]])
// console.log(toArray({})) //, [])

/*<----------------------------------------------------------------------------->*/

// Create a function that takes a number
// as an argument and returns true or false depending on whether
// the number is symmetrical or not. A number is symmetrical when it is the same as its reverse.
// function isSymmetrical(num) {
//     let rev = parseFloat(
//         num
//         .toString()
//         .split('')
//         .reverse()
//         .join('')
//     ) * Math.sign(num)
//     return rev === num ? true : false;
// }

// console.log(isSymmetrical(23)) //, false)
// console.log(isSymmetrical(9562)) //, false)
// console.log(isSymmetrical(10019)) //, false)
// console.log(isSymmetrical(1)) //, true)
// console.log(isSymmetrical(3223)) //, true)
// console.log(isSymmetrical(95559)) //, true)
// console.log(isSymmetrical(66566)) //, true)

/*<----------------------------------------------------------------------------->*/

// Create a function that accepts a string of space separated
// numbers and returns the highest and lowest number (as a string).

// function highLow(str) {
//     let retArr = []
//     let strArr = str.split(" ").sort((a, b) => {
//         return b - a
//     })
//     retArr.push(strArr[0], strArr.slice(-1)[0])
//     return retArr.join(' ')
// }

// console.log(highLow("4 5 29 54 4 0 -214 542 -64 1 -3 6 -6")) //, "542 -214")
// console.log(highLow("1 -1")) //, "1 -1")
// console.log(highLow("1 1")) //, "1 1")
// console.log(highLow("-1 -1")) //, "-1 -1")
// console.log(highLow("1 -1 0")) //, "1 -1")
// console.log(highLow("1 1 0")) //, "1 0")
// console.log(highLow("-1 -1 0")) //, "0 -1")
// console.log(highLow("42")) //, "42 42")

/*<----------------------------------------------------------------------------->*/

// Create a function which takes in a word and
// spells it out, by consecutively adding letters until the full word is completed.

//function spelling(str) {
//es5
// let strArr = [];
// for (let i = 0; i < str.length; i++) {
//     if (i === 0) {
//         strArr.push(str[i])
//     } else if (i > 0) {
//         let temp = str.slice(0, i)
//         strArr.push(temp + str[i])
//     }
// }
// return strArr

//es6
//return str.split('').map((c, i) => str.slice(0, i+1) );

// }

// console.log(spelling("bee")) //, ['b', 'be', 'bee'])
// console.log(spelling("cake")) //, ['c', 'ca', 'cak', 'cake'])
// console.log(spelling("happy")) //, ['h', 'ha', 'hap', 'happ', 'happy'])
// console.log(spelling("eagerly")) //, ['e', 'ea', 'eag', 'eage', 'eager', 'eagerl', 'eagerly'])
// console.log(spelling("believe")) //, ['b', 'be', 'bel', 'beli', 'belie', 'believ', 'believe'])

/*<----------------------------------------------------------------------------->*/

//Create a function that concatenates n input arrays, where n is variable.

//function concat(...args) {
// let merged = [].concat.apply([], args);
// return merged;
//   return [].concat(...args)
//}

// console.log(concat([1, 2, 3], [4, 5], [6, 7])) //, [1, 2, 3, 4, 5, 6, 7])
// console.log(concat([1], [2], [3], [4], [5], [6], [7])) //, [1, 2, 3, 4, 5, 6, 7])
// console.log(concat([1, 2], [3, 4])) //, [1, 2, 3, 4])
// console.log(concat([4, 4, 4, 4, 4])) //, [4, 4, 4, 4, 4])
// console.log(concat(['a'], ['b', 'c'])) //, ['a', 'b', 'c'])

/*<----------------------------------------------------------------------------->*/

// Write a function that takes a credit card number and only
// displays the last four characters. The rest of the card number must be replaced by ************.

//function cardHide(card) {
//es6
// let lastFour = card.slice(-4)
// let fullNumber = card.slice(0, -4).split('').map(a => a = '*').join('') + lastFour;
// return fullNumber;

//es5
// card = card.split('');
// for (let i = 0; i < card.length - 4; i++) {
//     card[i] = "*";
// }
// return card.join('');
//}

//console.log(cardHide("1234123456785678")) //, "************5678")
//console.log(cardHide("8754456321113213")) //, "************3213")
//console.log(cardHide("35123413355523")) //, "**********5523")

/*<----------------------------------------------------------------------------->*/

//The recursive function for this challenge should return the factorial of an inputted integer.
// function factorial(num) {
//     if (num === 0 || num === 1) {
//         return 1;
//     }
//     return num * factorial(num - 1);
// }

// console.log(factorial(7)) //, 5040)
// console.log(factorial(1)) //, 1)
// console.log(factorial(9)) //, 362880)
// console.log(factorial(2)) //, 2)

/*<----------------------------------------------------------------------------->*/

//Create a function that takes an object and returns the keys and values as separate arrays.

// function keysAndValues(obj) {
//     let arr = [

//     ]
//     let keys = Object.keys(obj);
//     let values = Object.values(obj);
//     arr.push(keys)
//     arr.push(values)
//     return arr
// }

// console.log(keysAndValues({
//     a: 1,
//     b: 2,
//     c: 3
// })) //, [["a", "b", "c"], [1, 2, 3]])
// console.log(keysAndValues({
//     a: "Apple",
//     b: "Microsoft",
//     c: "Google"
// })) //, [["a", "b", "c"], ["Apple", "Microsoft", "Google"]])
// console.log(keysAndValues({
//     key1: true,
//     key2: false,
//     key3: undefined
// })) //, [["key1", "key2", "key3"], [true, false, undefined]])
// console.log(keysAndValues({
//     1: null,
//     2: null,
//     3: null
// })) //, [["1", "2", "3"], [null, null, null]])
// console.log(keysAndValues({
//     key1: "cat",
//     key2: "dog",
//     key3: null
// })) //, [["key1", "key2", "key3"], ["cat", "dog", null]])

// /*<----------------------------------------------------------------------------->*/

//Create a function which validates whether a number n is exclusively within the
//bounds of lower and upper. Return false if n is not an integer.

// function intWithinBounds(n, lower, upper) {
//     if (!Number.isInteger(n) || n >= upper) {
//         return false;
//     } else {
//         return true;
//     }
// }

// console.log(intWithinBounds(6, 1, 6)) //, false)
// console.log(intWithinBounds(4.5, 3, 8)) //, false)
// console.log(intWithinBounds(-5, -10, 6)) //, true)
// console.log(intWithinBounds(4, 0, 0)) //, false)
// console.log(intWithinBounds(10, 9, 11)) //, true)
// console.log(intWithinBounds(6.3, 2, 6)) //, false)
// console.log(intWithinBounds(6.3, 2, 10)) //, false)
// console.log(intWithinBounds(9, 2, 3)) //, false)
// console.log(intWithinBounds(9, 9, 9)) //, false)
// console.log(intWithinBounds(-3, -5, -2)) //, true)
// console.log(intWithinBounds(-3, -5, -3)) //, false)
// console.log(intWithinBounds(-3, -10, 10)) //, true)
// console.log(intWithinBounds(0, -3, 3)) //, true)
// console.log(intWithinBounds(0, 0, 1)) //, true)
// console.log(intWithinBounds(7, 7, 12)) //, true)

/*----------------9/06/20  8:28 AM ---------------------*/

// Given a total due and an array representing the amount of
// change in your pocket, determine whether or not you are able to
// pay for the item. Change will always be represented in the following
// order: quarters, dimes, nickels, pennies.

//function changeEnough(change, amountDue) {
// let [quarters, dimes, nickels, pennies] = change;
// quarters *= .25;
// dimes *= .10;
// nickels *= .05;
// pennies *= .01;
// tlt = quarters + dimes + nickels + pennies;
// return tlt >= amountDue ? true : false;
//}

// console.log(changeEnough([2, 100, 0, 0], 14.11)) //, false);
// console.log(changeEnough([0, 0, 20, 5], 0.75)) //, true);
// console.log(changeEnough([30, 40, 20, 5], 12.55)) //, true);
// console.log(changeEnough([10, 0, 0, 50], 13.85)) //, false);
// console.log(changeEnough([1, 0, 5, 219], 19.99)) //, false);
// console.log(changeEnough([1, 0, 2555, 219], 127.75)) //, true);
// console.log(changeEnough([1, 335, 0, 219], 35.21)) //, true);

// /*<----------------------------------------------------------------------------->*/

//Create a function that takes two numbers as arguments (num, length) and returns an array
//of multiples of num up to length.

//function arrayOfMultiples(num, length) {
//es5
// let arr = []
// for (let i = 1; i <= length; i++) {
//     arr.push(num * i)
// }
// return arr

//es6
// return [...Array(length).keys()].map((e, i) => (e + 1) * num);
//}

// console.log(arrayOfMultiples(7, 5)) //, [7, 14, 21, 28, 35])
// console.log(arrayOfMultiples(12, 10)) //, [12, 24, 36, 48, 60, 72, 84, 96, 108, 120])
// console.log(arrayOfMultiples(17, 7)) //, [17, 34, 51, 68, 85, 102, 119])
// console.log(arrayOfMultiples(630, 14)) //, [630, 1260, 1890, 2520, 3150, 3780, 4410, 5040, 5670, 6300, 6930, 7560, 8190, 8820])
// console.log(arrayOfMultiples(140, 3)) //, [140, 280, 420])
// console.log(arrayOfMultiples(7, 8)) //, [7, 14, 21, 28, 35, 42, 49, 56])
// console.log(arrayOfMultiples(11, 21)) //, [11, 22, 33, 44, 55, 66, 77, 88, 99, 110, 121, 132, 143, 154, 165, 176, 187, 198, 209, 220, 231])

// /*<----------------------------------------------------------------------------->*/

// You're given a string of words. You need to find the word "Nemo", and return a string
// like this: "I found Nemo at [the order of the word you find nemo]!".
// If you can't find Nemo, return "I can't find Nemo :(".

// function findNemo(sentence) {
//     let nemo = 'Nemo'
//     let arr = sentence.split(' ')
//     for (let i = 0; i < arr.length; i++) {
//         if (arr[i] === nemo) {
//             return `I found Nemo at ${i +1}!`
//         }
//     }
//     return 'I can\'t find Nemo :('
// }

// console.log(findNemo("I am Ne mo Nemo !")) //, "I found Nemo at 5!")
// console.log(findNemo("N e m o is NEMO NeMo Nemo !")) //, "I found Nemo at 8!")
// console.log(findNemo("I am Nemo's dad Nemo senior .")) //, "I found Nemo at 5!")
// console.log(findNemo("Oh, hello !")) //, "I can't find Nemo :(")
// console.log(findNemo("Is it Nemos, Nemona, Nemoor or Garfield?")) //, "I can't find Nemo :(")
// console.log(findNemo("Nemo is a clown fish, he has white and orange stripes. Nemo , come back!")) //, "I found Nemo at 1!")

// // /*<----------------------------------------------------------------------------->*/

//Write a function that converts an object into an array of keys and values.

// function objectToArray(obj) {
//     let arr = Object.entries(obj);
//     return arr;
// }

// console.log(objectToArray({
//     D: 1,
//     B: 2,
//     C: 3
// })) //, [["D", 1], ["B", 2], ["C", 3]])

// console.log(objectToArray({
//     likes: 2,
//     dislikes: 3,
//     followers: 10
// })) //, [["likes", 2], ["dislikes", 3], ["followers", 10]])

// // /*<----------------------------------------------------------------------------->*/

//Create a function that moves all capital letters to the front of a word.

//function capToFront(s) {
//es5
// let arr = s.split('')
// let newStr = ''
// for (let i = 0; i < arr.length; i++) {
//     if (arr[i] === arr[i].toUpperCase()) {
//         newStr += arr[i]
//     }

// }
// for (let i = 0; i < arr.length; i++) {
//     if (arr[i] !== arr[i].toUpperCase()) {
//         newStr += arr[i]
//     }

// }
// return newStr;

//es6
//  return [...s].filter(x => x == x.toUpperCase()).join('') + [...s].filter(x => x == x.toLowerCase()).join('')
//}

//console.log(capToFront("hApPy")) //, "APhpy")
//console.log(capToFront("moveMENT")) //, "MENTmove")
//console.log(capToFront("aPPlE")) //, "PPEal")
//console.log(capToFront("shOrtCAKE")) //, "OCAKEshrt")

// // /*<----------------------------------------------------------------------------->*/

//Create a function to convert an array of percentages to their decimal equivalents.

//function convertToDecimal(perc) {
//es5
// let decArray = []
// for (let i = 0; i < perc.length; i++) {
//     decArray.push(parseFloat(perc[i]) / 100);
// }
// return decArray

//es6
//     let decArray = perc.map(a => parseFloat(a) / 100)
//     return decArray
// }

// console.log(convertToDecimal(["33%", "98.1%", "56.44%", "100%"])) //, [0.33, 0.981, 0.5644, 1])
// console.log(convertToDecimal(["45%", "32%", "97%", "33%"])) //, [0.45, 0.32, 0.97, 0.33])
// console.log(convertToDecimal(["1%", "2%", "3%"])) //, [0.01, 0.02, 0.03])

// // // /*<----------------------------------------------------------------------------->*/

// Given a list of cities and the distances between each pair of cities,
// what is the shortest possible route that visits each city and returns to the origin city?

// function paths(n) {
//     if (n === 1) {
//         return 1;
//     }
//     return n * paths(n - 1)
// }

// console.log(paths(1)) //, 1)
// console.log(paths(2)) //, 2)
// console.log(paths(3)) //, 6)
// console.log(paths(4)) //, 24)
// console.log(paths(5)) //, 120)
// console.log(paths(6)) //, 720)
// console.log(paths(7)) //, 5040)
// console.log(paths(8)) //, 40320)

// // // // /*<----------------------------------------------------------------------------->*/

// I'm trying to write a function to flatten an array of subarrays into one array.
// (Suppose I am unware there is a .flat() method in the Array prototype). In other words,
// I want to transform this: [[1, 2], [3, 4]] into [1, 2, 3, 4].

// function flatten(arr) {
//     //  arr2 = [];

//     let arr2 = [].concat.apply([], arr);
//     return arr2;
// }

// console.log(flatten([
//     [1, 2],
//     [3, 4]
// ])) //, [1, 2, 3, 4])
// console.log(flatten([
//     ['a', 'b'],
//     ['c', 'd']
// ])) //, ['a', 'b', 'c', 'd'])
// console.log(flatten([
//     [true, false],
//     [false, false]
// ])) //, [true, false, false, false])

// *<----------------------------------------------------------------------------->*/

// An array is special, if every even index contains an even number and every odd index contains
// an odd number. Create a function that returns true if an array is special, and false otherwise.

//function isSpecialArray(arr) {
//es5
// for (let i = 0; i < arr.length; i++) {
//     if ((i % 2 == 0 && arr[i] % 2 !== 0) || (i % 2 == 1 && arr[i] % 2 !== 1)) {
//         return false
//     }
// }
// return true

//es6
//return arr.every((element, index) => element % 2 === index % 2)
//}

// console.log(isSpecialArray([2, 7, 4, 9, 6, 1, 6, 3])) //, true)
// console.log(isSpecialArray([2, 7, 9, 1, 6, 1, 6, 3])) //, false)
// console.log(isSpecialArray([2, 7, 8, 8, 6, 1, 6, 3])) //, false)
// console.log(isSpecialArray([1, 1, 1, 2])) //, false)
// console.log(isSpecialArray([2, 2, 2, 2])) //, false)
// console.log(isSpecialArray([2, 1, 2, 1])) //, true)
// console.log(isSpecialArray([4, 5, 6, 7])) //, true)
// console.log(isSpecialArray([4, 5, 6, 7, 0, 5])) //, true)

/*----------------9/07/20  6:46 AM ---------------------*/

//Given an array of boxes, create a function that returns
//the total volume of all those boxes combined together. A box is represented by
//an array with three elements: length, width and height.

// function totalVolume(...boxes) {
//     //es6
//     let [first, second, ...rest] = boxes
//     let third = rest.flat();
//     if (second === undefined) {
//         return first.reduce((a, b) => a * b);
//     }
//     return third.length !== 0 ? first.reduce((a, b) => a * b) + second.reduce((a, b) => a * b) + third.reduce((a, b) => a * b) : first.reduce((a, b) => a * b) + second.reduce((a, b) => a * b);

// }

// console.log(totalVolume([4, 2, 4], [3, 3, 3], [1, 1, 2], [2, 1, 1])) // , 63)
//console.log(totalVolume([2, 2, 2], [2, 1, 1])) //, 10)
//console.log(totalVolume([1, 1, 1])) //, 1)
//console.log(totalVolume([5, 1, 10], [1, 9, 2])) //, 68)
//console.log(totalVolume([1, 1, 5], [3, 3, 1])) //, 14)

// *<----------------------------------------------------------------------------->*/

//Create a function that takes in an array (slot machine outcome) and returns
//true if all elements in the array are identical, and false otherwise. The array will contain 4 elements.

//function testJackpot(result) {
//es5
// for (let i = 0; i < result.length - 1; i++) {
//     if (result[i] !== result[i + 1]) {
//         return false;
//     }
// }
// return true;

//es6
//return result.every((val, i, arr) => val === arr[0])
//}

//console.log(testJackpot(['@', '@', '@', '@'])) //, true)
// console.log(testJackpot(['!', '!', '!', '!'])) //, true)
// console.log(testJackpot(['abc', 'abc', 'abc', 'abc'])) //, true)
// console.log(testJackpot(['karaoke', 'karaoke', 'karaoke', 'karaoke'])) //, true)
// console.log(testJackpot(['SS', 'SS', 'SS', 'SS'])) //, true)
// console.log(testJackpot([':(', ':)', ':|', ':|'])) //, false)
// console.log(testJackpot(['&&', '&', '&&&', '&&&&'])) //, false)
// console.log(testJackpot(['hee', 'heh', 'heh', 'heh'])) //, false)
//console.log(testJackpot(['SS', 'SS', 'SS', 'Ss'])) //, false)
//console.log(testJackpot(['SS', 'SS', 'Ss', 'Ss'])) //, false)

// *<----------------------------------------------------------------------------->*/

//Given a square matrix (i.e. same number of rows
//as columns), its trace is the sum of the entries in the main diagonal
//(i.e. the diagonal line from the top left to the bottom right).

//function trace(arr) {
//es5
// let trace = 0;
// for (y = 0; y < arr.length; y++) {
//     trace += arr[y][y];
// }
// return trace;

//es6
//return arr.reduce((acc, cur, i) => acc + (cur[i]), 0);
//}

//console.log(trace(
// [
//     [1, 4],
//     [4, 1]
// ]
//)) //, 2)

// console.log(trace(
//     [
//         [1, 2, 3],
//         [4, 5, 6],
//         [7, 8, 9]
//     ]
// )) //, 15)

// *<----------------------------------------------------------------------------->*/

//This Triangular Number Sequence
// is generated from a pattern of dots that form a triangle.
// The first 5 numbers of the sequence, or dots, are:

// function triangle(n) {
//     return n * (n + 1) / 2;
// }

// console.log(triangle(1)) //, 1)
// console.log(triangle(2)) //, 3)
// console.log(triangle(3)) //, 6)
// console.log(triangle(8)) //, 36)
// console.log(triangle(2153)) //, 2318781)

// *<----------------------------------------------------------------------------->*/

//Create a function that takes two numbers and a
//mathematical operator + - / * and will perform a calculation with the given numbers.

//function calculator(num1, operator, num2) {

//return num2 === 0 ? "Can't divide by 0!" : eval(`${num1}${operator}${num2}`)

// Better way without using eval()
// operations = {
//     '+': (a, b) => a + b,
//     '-': (a, b) => a - b,
//     '*': (a, b) => a * b,
//     '/': (a, b) => b ? a / b : "Can't divide by 0!"
// }
// return operations[operator](num1, num2)
//}

// console.log(calculator(2, '/', 2)) //, 1)
// console.log(calculator(10, '-', 7)) //, 3)
// console.log(calculator(2, '*', 16)) //, 32)
// console.log(calculator(2, '-', 2)) //, 0)
// console.log(calculator(15, '+', 26)) //, 41)
// console.log(calculator(2, '+', 2)) //, 4)
// console.log(calculator(2, "/", 0)) //, "Can't divide by 0!")

// *<----------------------------------------------------------------------------->*/

// Create a function that takes in a number as a string n and returns the number
// without trailing and leading zeros.
// Trailing Zeros are the zeros after a decimal point which don't affect the value
// (e.g. the last three zeros in 3.4000 and 3.04000).
// Leading Zeros are the zeros before a whole number which don't affect the value
// (e.g. the first three zeros in 000234 and 000230).

// function removeLeadingTrailing(n) {
//     let num = +n
//     return num.toString()
// }

// console.log(removeLeadingTrailing("00402")) //, "402")
// console.log(removeLeadingTrailing("03.1400")) //,, "3.14")
// console.log(removeLeadingTrailing("30")) //,, "30")
// console.log(removeLeadingTrailing("30.0000")) //,, "30")
// console.log(removeLeadingTrailing("24340")) //,, "24340")
// console.log(removeLeadingTrailing("0404040")) //,, "404040")
// console.log(removeLeadingTrailing("0")) //,, "0")
// console.log(removeLeadingTrailing("00")) //,, "0")
// console.log(removeLeadingTrailing("0.000000")) //,, "0")
// console.log(removeLeadingTrailing("0000.000")) //,, "0")
// console.log(removeLeadingTrailing("00.0001")) //,, "0.0001")
// console.log(removeLeadingTrailing("10000")) //,, "10000")
// console.log(removeLeadingTrailing("1345")) //,, "1345")
// console.log(removeLeadingTrailing("30.000020")) //,, "30.00002")
// console.log(removeLeadingTrailing("00200.1900001")) //,, "200.1900001")

// *<----------------------------------------------------------------------------->*/

//You just returned home to find your mansion has been robbed! Given an object of the stolen items,
//return the total amount of the burglary (number). If nothing was robbed, return the string "Lucky you!".

//function calculateLosses(obj) {
//es6
// return Object.keys(obj).length === 0 ? "Lucky you!" : Object.values(obj).reduce((a, b) => a + b)

//es5
//     let acc = 0
//     for (let val of Object.values(obj)) {
//         acc += val;
//     }
//     return acc === 0 ? "Lucky you!" : acc;
// }

// console.log(calculateLosses({
//     tv: 30,
//     skate: 20,
//     stereo: 50,
// })) //, 100)

// console.log(calculateLosses({
//     ring: 30000,
//     painting: 20000,
//     bust: 1,
// })) //, 50001)

// console.log(calculateLosses({
//     chair: 3500,
// })) //, 3500)

// console.log(calculateLosses({})) //, "Lucky you!")

// *<----------------------------------------------------------------------------->*/

//Create a function that takes a number (step) as an argument and returns the amount
//of boxes in that step of the sequence.
// Step 0: Start with 0
// Step 1: Add 3
// Step 2: Subtract 1
// Repeat Step 1 & 2 ...

//function boxSeq(step) {
//es5
// let count = 0;
// if (step === 0) {
//     return 0;
// }
// for (let i = 1; i <= step; i++) {
//     if (i % 2 === 0) {
//         count -= 1;
//     }
//     if (i % 2 === 1) {
//         count += 3;
//     }
// }
// return count;

//Better approach
//  return step + (step % 2 * 2)
//}

// console.log(boxSeq(5)) //, 7)
// console.log(boxSeq(0)) //, 0)
// console.log(boxSeq(6)) //, 6)
// console.log(boxSeq(99)) //, 101)
// console.log(boxSeq(2)) //, 2)
// console.log(boxSeq(1)) //, 3)

// *<----------------------------------------------------------------------------->*/

// Create a function that validates whether three given integers form a
// Pythagorean triplet. The sum of the squares of the two smallest integers must
// equal the square of the largest number to be validated.

// function isTriplet(n1, n2, n3) {
//     let arr = [n1, n2, n3].sort((a, b) => a - b)
//     let [s, m, l] = arr;
//     let one = Math.pow(s, 2) + Math.pow(m, 2);
//     let two = Math.pow(l, 2)
//     return one === two ? true : false
// }

// console.log(isTriplet(3, 4, 5)) //, true)
// console.log(isTriplet(1, 2, 3)) //, false)
// console.log(isTriplet(3, 18, 8)) //, false)
// console.log(isTriplet(7, 12, 7)) //, false)
// console.log(isTriplet(13, 5, 12)) //, true)
// console.log(isTriplet(12, 20, 18)) //, false)
// console.log(isTriplet(17, 14, 2)) //, false)
// console.log(isTriplet(6, 15, 12)) //, false)
// console.log(isTriplet(60, 61, 11)) //, true)
// console.log(isTriplet(7, 13, 15)) //, false)
// console.log(isTriplet(12, 18, 7)) //, false)
// console.log(isTriplet(8, 17, 15)) //, true)
//console.log(isTriplet(3120, 79, 3121)) //, true)

// *<----------------------------------------------------------------------------->*/

//Create the function that takes an array with objects and returns the sum of people's budgets.

//function getBudgets(arr) {
//es5
// let sum = 0
// for (let i = 0; i < arr.length; i++) {
//     sum += arr[i].budget
// }
// return sum;

//es6
// return arr.reduce((a, v) => a + v.budget, 0);
//}

// console.log(getBudgets([{
//     name: "John",
//     age: 21,
//     budget: 23000
// }, {
//     name: "Steve",
//     age: 32,
//     budget: 40000
// }, {
//     name: "Martin",
//     age: 16,
//     budget: 2700
// }])) //, 65700)
// console.log(getBudgets([{
//     name: "John",
//     age: 21,
//     budget: 29000
// }, {
//     name: "Steve",
//     age: 32,
//     budget: 32000
// }, {
//     name: "Martin",
//     age: 16,
//     budget: 1600
// }])) //, 62600)
// console.log(getBudgets([{
//     name: "John",
//     age: 21,
//     budget: 19401
// }, {
//     name: "Steve",
//     age: 32,
//     budget: 12321
// }, {
//     name: "Martin",
//     age: 16,
//     budget: 1204
// }])) //, 32926)
// console.log(getBudgets([{
//     name: "John",
//     age: 21,
//     budget: 10234
// }, {
//     name: "Steve",
//     age: 32,
//     budget: 21754
// }, {
//     name: "Martin",
//     age: 16,
//     budget: 4935
// }])) //, 36923)

/*----------------9/07/20  6:46 AM ---------------------*/

// Create a function that squares every digit of a number.
// function squareDigits(n) {
//     let arr = n.toString().split('').map(a => Math.pow(parseInt(a), 2)).join('')
//     return parseInt(arr)

// }

// console.log(squareDigits(9119)) //, 811181)
// console.log(squareDigits(8726)) //, 6449436)
// console.log(squareDigits(9763)) //, 8149369)
// console.log(squareDigits(2230)) //, 4490)
// console.log(squareDigits(2797)) //, 4498149)
// console.log(squareDigits(233)) //, 499)
// console.log(squareDigits(7437)) //, 4916949)
// console.log(squareDigits(2483)) //, 416649)
// console.log(squareDigits(5742)) //, 2549164)
// console.log(squareDigits(5636)) //, 2536936)
// console.log(squareDigits(841)) //, 64161)

// *<----------------------------------------------------------------------------->*/

//Given an array of arrays, return a new array of arrays
// containing every element, except for the outer elements.

// function peelLayerOff(arr) {
//     arr.shift();
//     arr.pop();
//     for (i = 0; i < arr.length; i++) {
//         arr[i].shift();
//         arr[i].pop();
//     }
//     return arr;
// }

// console.log(peelLayerOff([
//     ['a', 'b', 'c', 'd'],
//     ['e', 'f', 'g', 'h'],
//     ['i', 'j', 'k', 'l'],
//     ['m', 'n', 'o', 'p']
// ])) //, [
// // 	['f', 'g'],
// // 	['j', 'k']
// // ])

// *<----------------------------------------------------------------------------->*/

//Return the sum of all items in an array, where each item is multiplied by
//its index (zero-based). For empty arrays, return 0.

// function indexMultiplier(arr) {
//     return arr.length === 0 ? 0 : arr.map((a, i) => a * i).reduce((a, b) => a + b)
// }

// console.log(indexMultiplier([9, 3, 7, -7])) //, -4)

// *<----------------------------------------------------------------------------->*/

//Create a function that takes three arguments a, b, c and
// returns the sum of the numbers that are evenly divided by c from the range a, b inclusive.

// function evenlyDivisible(a, b, c) {
//     let sum = 0;
//     for (let i = a; i <= b; i++) {
//         if (i % c === 0) {
//             sum += i
//         }
//     }
//     return sum;
// }

// console.log(evenlyDivisible(1, 10, 2)) //, 30)
// console.log(evenlyDivisible(1, 10, 3)) //, 18)
// console.log(evenlyDivisible(0, 12, 3)) //, 30)
// console.log(evenlyDivisible(-10, -1, 2)) //, -30)
// console.log(evenlyDivisible(-10, -1, 3)) //, -18)
// console.log(evenlyDivisible(1, 10, 20)) //, 0)
// console.log(evenlyDivisible(-10, 10, 2)) //, 0)

// *<----------------------------------------------------------------------------->*/

// A factor chain is an array where each previous element is
// a factor of the next consecutive element. The following is a factor chain:
//[3, 6, 12, 36]
// 3 is a factor of 6
// 6 is a factor of 12
// 12 is a factor of 36
//Create a function that determines whether or not an array is a factor chain.

// function factorChain(arr) {
//     for (let i = 0; i < arr.length; i++) {
//         let temp;
//         i === 0 ? temp = arr[0] : temp = arr[i - +1]
//         if (arr[i] % temp !== 0) {
//             return false;
//         }
//     }
//     return true
// }

// console.log(factorChain([1, 2, 4, 8, 16, 32])) //, true)
// console.log(factorChain([1, 1, 1, 1, 1, 1])) //, true)
// console.log(factorChain([2, 4, 6, 7, 12])) //, false)
// console.log(factorChain([10, 1])) //, false)
// console.log(factorChain([10, 20, 30, 40])) //, false)
// console.log(factorChain([10, 20, 40])) //, true)
// console.log(factorChain([1, 1, 1, 1, 7, 49])) //, true)

// *<----------------------------------------------------------------------------->*/

//Create a function that takes an integer n and reverses it.

// function rev(n) {
//     return Math.abs(n).toString().split('').reverse().join('');
// }

// console.log(rev(215)) //, "512")
// console.log(rev(122225)) //, "522221")
// console.log(rev(215)) //, "512")
// console.log(rev(-215)) //, "512", "Should work with negative numbers.")
// console.log(rev(-2152)) //, "2512", "Should work with negative numbers.")
// console.log(rev(-122157)) //, "751221", "Should work with negative numbers.")
// console.log(rev(666)) //, "666", "Should work if all digits are the same.")
// console.log(rev(999)) //, "999", "Should work if all digits are the same.")

// *<----------------------------------------------------------------------------->*/

//Mary wants to run a 25-mile marathon. When she attempts to sign up for the marathon,
// she notices the sign-up sheet doesn't directly state the marathon's length. Instead,
// the marathon's length is listed in small, different portions. Help Mary find out how long the marathon actually is.
//Return true if the marathon is 25 miles long, otherwise, return false.

// function marathonDistance(d) {
//     if (d.length === 0) {
//         return false;
//     }
//     return d.reduce((a, b) => Math.abs(a) + b) === 25 ? true : false;
// }

// console.log(marathonDistance([1, 2, 3, 4])) //, false)
// console.log(marathonDistance([-6, 15, 4])) //, true)
// console.log(marathonDistance([1, 9, 5, 8, 2])) //, true)
// console.log(marathonDistance([9, 7, 6, 5])) //, false)
// console.log(marathonDistance([4, 6, 8, 9, -4])) //, false)
// console.log(marathonDistance([-20, 9, -10, -11])) //, false)
// console.log(marathonDistance([-9, 15, 1])) //, true)
// console.log(marathonDistance([])) //, false)
// console.log(marathonDistance([])) //, false)

/*----------------9/11/20  8:41 AM ---------------------*/
// "What about Brutus, is he gone?" asks your spouse.
// Brutus is right in front of you but you never liked him and iguanas can easily disappear...
// Given an object of the stolen items, add the name of the pet and a value and return
// the object with his name on it.

// function addName(obj, name, value) {
//     obj[name] = value
//     return obj;
// };

// console.log(addName({}, "Brutus", 300)) //, {Brutus: 300})
// console.log(addName({
//     piano: 500
// }, "Brutus", 400)) //, {piano: 500, Brutus: 400})
// console.log(addName({
//     piano: 500,
//     stereo: 300
// }, "Caligula", 440)) //, {piano: 500,  stereo: 300, Caligula: 440})

// *<----------------------------------------------------------------------------->*/

//Create a function which concantenates the number 7 to the end of every chord in
//an array. Ignore all chords which already end with 7.

// function jazzify(arr) {
//     return arr.map((a) => {
//         if (a.includes('7')) {
//             return a
//         } else {
//             return `${a}7`
//         }
//     })

// }

// console.log(jazzify(['G', 'F', 'C'])) //, ['G7', 'F7', 'C7'])
// console.log(jazzify(['Dm', 'G', 'E', 'A'])) //, ['Dm7', 'G7', 'E7', 'A7'])
// console.log(jazzify(['F7', 'E7', 'A7', 'Ab7', 'Gm7', 'C7'])) //, ['F7', 'E7', 'A7', 'Ab7', 'Gm7', 'C7'])
// console.log(jazzify(['G', 'C7'])) //, ['G7', 'C7'])
// console.log(jazzify([])) //, [])

// *<----------------------------------------------------------------------------->*/

//Count the amount of ones in the binary representation of an integer.
//So for example, since 12 is '1100' in binary, the return value should be 2.

// function countOnes(i) {
//     i = i.toString(2);
//     return i.split('1').length - 1;
// }

// console.log(countOnes(12)) //, 2)
// console.log(countOnes(0)) //, 0)
// console.log(countOnes(100)) //, 3)
// console.log(countOnes(101)) //, 4)
// console.log(countOnes(999)) //, 8)
// console.log(countOnes(1e9)) //, 13)
// console.log(countOnes(123456789)) //, 16)
// console.log(countOnes(1234567890)) //, 12)

// *<----------------------------------------------------------------------------->*/

//You are in charge of the barbecue grill. A vegetarian skewer is a skewer that has
//only vegetables (-o). A non-vegetarian skewer is a skewer with at least one piece of meat (-x).

//For example, the grill below has 4 non-vegetarian skewers and 1 vegetarian skewer (the one in the middle).

// ["--xo--x--ox--",
// "--xx--x--xx--",
// "--oo--o--oo--",      <<< vegetarian skewer
// "--xx--x--ox--",
// "--xx--x--ox--"]

//Given a BBQ grill, write a function that returns [# vegetarian skewers,
//# non-vegetarian skewers]. For example above, the function should return [1, 4].

// function bbqSkewers(grill) {
//     let meat = grill.filter(a => a.includes('x'))
//     let veg = grill.length - meat.length;
//     return [veg, meat.length]
// }

// console.log(bbqSkewers(
//     ["--oooo-ooo--",
//         "--xx--x--xx--",
//         "--o---o--oo--",
//         "--xx--x--ox--",
//         "--xx--x--ox--"
//     ]
// )) //, [2, 3])

// console.log(bbqSkewers(
//     ["--oooo-ooo--",
//         "--xxxxxxxx--",
//         "--o---",
//         "-o-----o---x--",
//         "--o---o-----"
//     ]
// )) //, [3, 2])

// console.log(bbqSkewers(
//     ["--oooo-ooo--",
//         "--ooooooo--",
//         "--o---",
//         "-o-----o---x--",
//         "--o-oooo-----"
//     ]
// )) //, [4, 1])

/*----------------9/12/20  8:41 AM ---------------------*/

//Create a function that takes a string and replaces the vowels with another character.

//function replaceVowel(word) {
// a = 1
// e = 2
// i = 3
// o = 4
// u = 5
//	return word.replace(/[aeiou]/g, (v) => "aeiou".indexOf(v) + 1);
//}

// console.log(replaceVowel("karachi")); //, "k1r1ch3")
// console.log(replaceVowel("dang")); //, "d1ng")
// console.log(replaceVowel("aen")); //, "12n")
// console.log(replaceVowel("chembur")); //, "ch2mb5r")
// console.log(replaceVowel("khandbari")); //, "kh1ndb1r3")
// console.log(replaceVowel("thamel")); //, "th1m2l")

// *<----------------------------------------------------------------------------->*/

//To train for an upcoming marathon, Johnny goes on one long-distance run each Saturday.
//He wants to track how often the number of miles he runs this Saturday exceeds the number
//of miles run the previous Saturday. This is called a progress day.
//Create a function that takes in an array of miles run every Saturday and returns
//Johnny's total number of progress days.

//function progressDays(runs) {
//es5
// let prog = 0;
// for (let i = 0; i < runs.length; i++) {
// 	if (runs[i] < runs[i + 1]) {
// 		prog += 1;
// 	}
// }
// return prog;

//es6
//return runs.filter((e, i, a) => e < a[i + 1]).length;
//}

// console.log(progressDays([3, 4, 1, 2])); //, 2);
// console.log(progressDays([10, 11, 12, 9, 10])); //, 3);
// console.log(progressDays([6, 5, 4, 3, 2, 9])); //, 1);
// console.log(progressDays([9, 9])); //, 0);
// console.log(progressDays([12, 11, 10, 12, 11, 13])); //, 2);

// *<----------------------------------------------------------------------------->*/

//Create a function that takes an array of items, removes all duplicate items and
// returns a new array in the same sequential order as the old array (minus duplicates).

// function removeDups(arr) {
// 	let unique = [...new Set(arr)];
// 	return unique;
// }

// console.log(removeDups(["John", "Taylor", "John"])); //, ['John', 'Taylor'])
// console.log(removeDups(["John", "Taylor", "John", "john"])); //, ['John', 'Taylor', 'john'])
// console.log(
// 	removeDups([
// 		"javascript",
// 		"python",
// 		"python",
// 		"ruby",
// 		"javascript",
// 		"c",
// 		"ruby",
// 	])
// ); //, ['javascript', 'python', 'ruby', 'c'])
// console.log(removeDups([1, 2, 2, 2, 3, 2, 5, 2, 6, 6, 3, 7, 1, 2, 5])); //, [1, 2, 3, 5, 6, 7])
// console.log(removeDups(["#", "#", "%", "&", "#", "$", "&"])); //, ['#', '%', '&', '$'])
// console.log(removeDups([3, "Apple", 3, "Orange", "Apple"])); //, [3, 'Apple', 'Orange'])

// *<----------------------------------------------------------------------------->*/

//Given an array of numbers, write a function that returns an array that...
//Has all duplicate elements removed.
//Is sorted from least to greatest value.

// function uniqueSort(arr) {
// 	arr.sort((a, b) => {
// 		return a - b;
// 	});
// 	let unique = arr.filter((el, index, arr) => {
// 		return arr.indexOf(el) === index;
// 	});
// 	return unique;
// }

// console.log(uniqueSort([1, 5, 8, 2, 3, 4, 4, 4, 10]));
// // [1, 2, 3, 4, 5, 8, 10]
// //);

// console.log(uniqueSort([1, 2, 5, 4, 7, 7, 7]));
// //   [1, 2, 4, 5, 7]
// // );

// console.log(uniqueSort([7, 6, 5, 4, 3, 2, 1, 0, 1]));
// //   [0, 1, 2, 3, 4, 5, 6, 7]
// // );

// *<----------------------------------------------------------------------------->*/

//Write a function that reverses all the words in a sentence that start with a particular letter.
//Reverse the words themselves, not the entire sentence.
//All characters in the sentence will be in lower case.

// function specialReverse(s, c) {
// 	return s
// 		.split(" ")
// 		.map((x) => (x[0] == c ? x.split("").reverse().join("") : x))
// 		.join(" ");
// }

// console.log(specialReverse("word searches are super fun", "s")); //, 'word sehcraes are repus fun')
//console.log(specialReverse("first man to walk on the moon", "m")); //, 'first nam to walk on the noom')
//console.log(specialReverse("peter piper picked pickled peppers", "p")); //, 'retep repip dekcip delkcip sreppep')
//console.log(specialReverse("he went to climb mount everest", "p")); //, 'he went to climb mount everest')

// *<----------------------------------------------------------------------------->*/

//Create a function that takes a string as an argument and converts the
//first character of each word to uppercase. Return the newly formatted string.

// function makeTitle(str) {
// 	return str
// 		.split(" ")
// 		.map((w) => w[0].toUpperCase() + w.slice(1))
// 		.join(" ");
// }

// console.log(makeTitle("I am a title")); //, "I Am A Title")
// console.log(makeTitle("I AM A TITLE")); //, "I AM A TITLE")
// console.log(makeTitle("i aM a tITLE")); //, "I AM A TITLE")
// console.log(makeTitle("the first letter of every word is capitalized")); //, "The First Letter Of Every Word Is Capitalized")
// console.log(makeTitle("I Like Pizza")); //, "I Like Pizza")
// console.log(makeTitle("Don't count your ChiCKens BeFore They HatCh")); //, "Don't Count Your ChiCKens BeFore They HatCh")
// console.log(makeTitle("All generalizations are false, including this one")); //, "All Generalizations Are False, Including This One")
// console.log(
// 	makeTitle("Me and my wife lived happily for twenty years and then we met.")
// ); //, "Me And My Wife Lived Happily For Twenty Years And Then We Met.")
// console.log(makeTitle("There are no stupid questions, just stupid people.")); //, "There Are No Stupid Questions, Just Stupid People.")
// console.log(makeTitle("1f you c4n r34d 7h15, you r34lly n33d 2 g37 l41d")); //, "1f You C4n R34d 7h15, You R34lly N33d 2 G37 L41d")
// console.log(makeTitle("PIZZA PIZZA PIZZA")); //, "PIZZA PIZZA PIZZA")

// *<----------------------------------------------------------------------------->*/

//Write a function that takes an integer i and returns an
//integer with the integer backwards followed by the original integer.
//We reverse 123 to get 321 and then add 123 to the end, resulting in 321123.
// function reverseAndNot(i) {
// 	let digits = i.toString().split("").reverse().join("") + i;
// 	return +digits;
// }

// console.log(reverseAndNot(123)); //, 321123)
// console.log(reverseAndNot(123456789)); //, 987654321123456789)
// console.log(reverseAndNot(496)); //, 694496)
// console.log(reverseAndNot(307)); //, 703307)
// console.log(reverseAndNot(500)); //, 5500)

// *<----------------------------------------------------------------------------->*/

//Given a string of numbers separated by a comma and space, return the product of the numbers.

// function multiplyNums(nums) {
// 	return nums
// 		.split(", ")
// 		.map((a) => +a)
// 		.reduce((a, b) => a * b);
// }

// console.log(multiplyNums("2, 3")); //, 6)
// console.log(multiplyNums("1, 2, 3, 4")); //, 24)
// console.log(multiplyNums("54, 75, 453, 0")); //, 0)
// console.log(multiplyNums("10, -2")); //, -20)
// console.log(multiplyNums("-26, 1, -27, -12, -19")); //, 160056)
// console.log(multiplyNums("16, 8")); //, 128)
// console.log(multiplyNums("-27, -14, -28, 13, -17")); //, 2339064)

// *<----------------------------------------------------------------------------->*/

//Create a function that takes an array of names and returns an array
//where only the first letter of each name is capitalized.

// function capMe(arr) {
// 	let newArr = [];
// 	for (let i = 0; i < arr.length; i++) {
// 		let lower = arr[i].toLowerCase();
// 		let el = lower.charAt(0).toUpperCase();
// 		let el2 = el + lower.slice(1);
// 		newArr.push(el2);
// 	}
// 	return newArr;
// }

// console.log(capMe(["mavis", "senaida", "letty"])); //, ['Mavis', 'Senaida', 'Letty'])
// console.log(capMe(["samuel", "MABELLE", "letitia", "meridith"])); //, ['Samuel', 'Mabelle', 'Letitia', 'Meridith'])
// console.log(capMe(["Slyvia", "Kristal", "Sharilyn", "Calista"])); //, ['Slyvia', 'Kristal', 'Sharilyn', 'Calista'])
// console.log(capMe(["krisTopher", "olIva", "herminiA"])); //, ['Kristopher', 'Oliva', 'Herminia'])
// console.log(capMe(["luke", "marsha", "stanford"])); //, ['Luke', 'Marsha', 'Stanford'])
// console.log(capMe(["kara"])); //, ['Kara'])
// console.log(capMe(["mARIANN", "jOI", "gEORGEANN"])); //, ['Mariann', 'Joi', 'Georgeann'])

// *<----------------------------------------------------------------------------->*/

//Create a function that returns the value of x (the "unknown" in the equation).
// Each equation will be formatted like this:
//x + 6 = 12

// function solve(eq) {
// 	const [x, sign, leftValue, equals, rightValue] = eq.split(" ");
// 	if (sign == "+") {
// 		return Number(rightValue) - Number(leftValue);
// 	} else {
// 		return Number(rightValue) + Number(leftValue);
// 	}
// }

// console.log(solve("x + 43 = 50")); //, 7)
// console.log(solve("x - 9 = 10")); //,, 19)
// console.log(solve("x + 300 = 100")); //,, -200)
// console.log(solve("x - 0 = 0")); //, 0)
// console.log(solve("x + 188 = 866")); //, 678)
// console.log(solve("x + -500 = -200")); //, 300)

// *<----------------------------------------------------------------------------->*/

// Create a function that takes a string, checks if it has the same number
// of x's and o's and returns either true or false.
// Return a boolean value (true or false).
// The string can contain any character.
// When no x and no o are in the string, return true.

// function XO(str) {
// 	let xs = 0;
// 	let os = 0;
// 	str.split("").map((a) => {
// 		if (a.toLowerCase() === "x") {
// 			xs++;
// 		} else if (a.toLowerCase() === "o") {
// 			os++;
// 		}
// 	});
// 	return xs === os ? true : false;
// }

// console.log(XO("ooxx")); //, true);
// console.log(XO("xooxx")); //, false);
// console.log(XO("ooxXm")); //, true);
// console.log(XO("zpzpzpp")); //, true);
// console.log(XO("zzoo")); //, false);
// console.log(XO("Xo")); //, true);
// console.log(XO("x")); //, false);
// console.log(XO("o")); //, false);
// console.log(XO("xxxoo")); //, false);
// console.log(XO("")); //, true);

// *<----------------------------------------------------------------------------->*/

// In this challenge you will be given an array similar to the following:
// [[3], 4, [2], [5], 1, 6]

// In words, elements of the array are either an integer or an array containing a single integer.
// We humans can clearly see that this array can reasonably be sorted
// according to "the content of the elements" as:
// [1, [2], [3], 4, [5], 6]

// Create a function that, given an array similar to the above,
// sorts the array according to the "content of the elements".

// function sortIt(arr) {
// 	return arr.sort((a, b) => a - b);
// }

// console.log(sortIt([4, 1, 3])); //, [1, 3, 4])
// console.log(sortIt([[4], [1], [3]])); //, [[1], [3], [4]])
// console.log(sortIt([4, [1], 3])); //, [[1], 3, 4])
// console.log(sortIt([[4], 1, [3]])); //, [1, [3], [4]])
// console.log(sortIt([[3], 4, [2], [5], 1, 6])); //, [1, [2], [3], 4, [5], 6])
// console.log(sortIt([[3], 7, [9], [5], 1, 6])); //, [1, [3], [5], 6, 7, [9]])
// console.log(sortIt([[3], 7, [9], [5], 1, 6, [0]])); //, [[0], 1, [3], [5], 6, 7, [9]])

// *<----------------------------------------------------------------------------->*/

// Create a function which validates whether a 3 character string is a vowel sandwich.
// In order to have a valid sandwich, the string must satisfy the following rules:
// The first and last characters must be a consonant.
// The character in the middle must be a vowel.

// function isVowelSandwich(str) {
// 	let vowel = new RegExp(/[aeiou]+/gi);
// 	if (str.length !== 3) {
// 		return false;
// 	}
// 	if (str.charAt(0).match(vowel) !== null) {
// 		return false;
// 	}
// 	if (str.charAt(1).match(vowel) === null) {
// 		return false;
// 	}
// 	if (str.charAt(2).match(vowel) !== null) {
// 		return false;
// 	}

// 	return true;
// }

// console.log(isVowelSandwich("nacr")); //, false);
// console.log(isVowelSandwich("phlsf")); //, false);
// console.log(isVowelSandwich("iah")); //, false);
// console.log(isVowelSandwich("to")); //, false);
// console.log(isVowelSandwich("lib")); //, true);
// console.log(isVowelSandwich("oiu")); //, false);

/*----------------9/15/20  11:51 AM ---------------------*/

//Create a function that takes a single character
//as an argument and returns the char code of its lowercased / uppercased counterpart.

// function counterpartCharCode(char) {
//     return char === char.toUpperCase() ? char.toLowerCase().charCodeAt() : char.toUpperCase().charCodeAt()
// }

// // Normal letters
// console.log(counterpartCharCode('a')) //, 65)
// console.log(counterpartCharCode('A')) //, 97)
// console.log(counterpartCharCode('l')) //, 76)
// console.log(counterpartCharCode('L')) //, 108)
// console.log(counterpartCharCode('z')) //, 90)
// console.log(counterpartCharCode('Z')) //, 122)

// // Accented / weird letters
// console.log(counterpartCharCode('è')) //, 200)
// console.log(counterpartCharCode('È')) //, 232)
// console.log(counterpartCharCode('Œ')) //, 339)
// console.log(counterpartCharCode('œ')) //, 338)
// console.log(counterpartCharCode('Ⱥ')) //, 11365)
// console.log(counterpartCharCode('ⱥ')) //, 570)

// // These don't have a counterpart, you should return the input's char code
// console.log(counterpartCharCode('5')) //, 53)
// console.log(counterpartCharCode('$')) //, 36)

// *<----------------------------------------------------------------------------->*/

//Create a function that takes an array of numbers and returns
//the sum of the two lowest positive numbers.

// function sumTwoSmallestNums(arr) {
//     let sum = arr.filter((a) => a === Math.abs(a)).sort((a, b) => a - b).reduce((cur, prev, index, arr) => {
//         return arr[0] + arr[1]
//     })
//     return sum
// }

// console.log(sumTwoSmallestNums([19, 5, 42, 2, 77])) //, 7);
// console.log(sumTwoSmallestNums([10, 343445353, 3453445, 3453545353453])) //, 3453455);
// console.log(sumTwoSmallestNums([2, 9, 6, -1])) //, 8);
// console.log(sumTwoSmallestNums([879, 953, 694, -847, 342, 221, -91, -723, 791, -587])) //, 563);
// console.log(sumTwoSmallestNums([3683, 2902, 3951, -475, 1617, -2385])) //, 4519);
// console.log(sumTwoSmallestNums([280, 134, 108])) //, 242);
// console.log(sumTwoSmallestNums([280, 134, 108, 1])) //, 109);
// console.log(sumTwoSmallestNums([321, 406, -176])) //, 727);
// console.log(sumTwoSmallestNums([1, 1, 1, 1])) //, 2);
// console.log(sumTwoSmallestNums([-1, -1, 1, 1])) //, 2);

// *<----------------------------------------------------------------------------->*/

//Create a function that takes a string and returns the middle
//character(s). If the word's length is odd, return the middle
//character. If the word's length is even, return the middle two characters.

// function getMiddle(str) {
//     let position;
//     let length;
//     if (str.length % 2 == 1) {
//         position = str.length / 2;
//         length = 1;
//     } else {
//         position = str.length / 2 - 1;
//         length = 2;
//     }
//     return str.substring(position, position + length)
// }

// //console.log(getMiddle("test")) //, "es")
// console.log(getMiddle("testing")) //, "t")
// //console.log(getMiddle("middle")) //, "dd")
// // console.log(getMiddle("A")) //, "A")
// // console.log(getMiddle("inhabitant")) //, "bi")
// console.log(getMiddle("brown")) //, "o")
// // console.log(getMiddle("pawn")) //, "aw")
// // console.log(getMiddle("cabinet")) //, "i")
// // console.log(getMiddle("fresh")) //, "e")
// // console.log(getMiddle("shorts")) //, "or")

// *<----------------------------------------------------------------------------->*/

//Create a function to check whether the given parameter is an Object or not.

// function isObject(value) {
//     return value && typeof value === 'object' ? true : false;
// }

// console.log(isObject(new Date())) //, true)
// console.log(isObject("12/12/2011")) //, false)
// console.log(isObject(null)) //, false)
// console.log(isObject([1, 2, 3])) //, true)
// console.log(isObject({})) //, true)

// *<----------------------------------------------------------------------------->*/

// Create a function that takes three integer arguments (a, b, c) 
// and returns the amount of integers which are of equal value.

// function equal(a, b, c) {
//     if (a === b && a === c) {
//         return 3;
//     }
//     if (a === b || a === c || b === c) {
//         return 2;
//     }
//     return 0;
// }

// console.log(equal(2, 3, 4)) //, 0, "All values are differents");
// console.log(equal(7, 3, 7)) //, 2, "Two values are equal");
// console.log(equal(4, 4, 4)) //, 3, "All 3 values are equal");
// console.log(equal(7, 3, 4)) //, 0, "All values are differents");
// console.log(equal(3, 3, 6)) //, 2, "Two values are equal");
// console.log(equal(1, 1, 1)) //, 3, "All 3 values are equal");
// console.log(equal(1, 7, 6)) //, 0, "All values are differents");
// console.log(equal(7, 7, 7)) //, 3, "All 3 values are equal");

/*----------------9/16/20  11:34 AM ---------------------*/



// // Create a function that takes an array of numbers between 1
// // and 10 (excluding one number) and returns the missing number.

// function missingNum(arr) {
//     var sum = arr.reduce((a, b) => a + b, 0);
//     return 55 - sum;
// }

// console.log(missingNum([1, 2, 3, 4, 6, 7, 8, 9, 10])) //, 5)
//console.log(missingNum([7, 2, 3, 6, 5, 9, 1, 4, 8])) //, 10)
//console.log(missingNum([7, 2, 3, 9, 4, 5, 6, 8, 10])) //, 1)
//console.log(missingNum([10, 5, 1, 2, 4, 6, 8, 3, 9])) //, 7)
//console.log(missingNum([1, 7, 2, 4, 8, 10, 5, 6, 9])) //, 3)

// *<----------------------------------------------------------------------------->*/

//Create a function that returns true if an asterisk * is inside a box.

// function inBox(arr) {
//     let inBox = false
//     arr.map((a) => {
//         if (a.includes('*')) {
//             inBox = true
//         }
//     })
//     return inBox;
// }

// console.log(inBox([
//     "###",
//     "# #",
//     "###"
// ])) //, false)

// console.log(inBox([
//     "####",
//     "#  #",
//     "#  #",
//     "####"
// ])) //, false)

// console.log(inBox([
//     "#####",
//     "#   #",
//     "#   #",
//     "#   #",
//     "#####"
// ])) //, false)

// console.log(inBox([
//     "###",
//     "#*#",
//     "###"
// ])) //, true)

// console.log(inBox([
//     "####",
//     "# *#",
//     "#  #",
//     "####"
// ])) //, true)

// console.log(inBox([
//     "#####",
//     "#  *#",
//     "#   #",
//     "#   #",
//     "#####"
// ])) //, true)

// console.log(inBox([
//     "#####",
//     "#   #",
//     "# * #",
//     "#   #",
//     "#####"
// ])) //, true)

// console.log(inBox([
//     "#####",
//     "#   #",
//     "#   #",
//     "# * #",
//     "#####"
// ])) //, true)

// console.log(inBox([
//     "#####",
//     "#*  #",
//     "#   #",
//     "#   #",
//     "#####"
// ])) //, true)

// *<----------------------------------------------------------------------------->*/

// Create a function that takes a string as an argument and returns a coded 
// (h4ck3r 5p34k) version of the string.
// In order to work properly, the function should 
// replace all "a"s with 4, "e"s with 3, "i"s with 1, "o"s with 0, and "s"s with 5.

// function hackerSpeak(str) {
//     let a = /a/gi;
//     let e = /e/gi;
//     let i = /i/gi;
//     let o = /o/gi;
//     let s = /s/gi;
//     let first = str.replace(a, 4)
//     let second = first.replace(e, 3)
//     let third = second.replace(i, 1)
//     let fourth = third.replace(o, 0)
//     let fifth = fourth.replace(s, 5)
//     return fifth
// }

// console.log(hackerSpeak("javascript is cool")) //, "j4v45cr1pt 15 c00l")
// console.log(hackerSpeak("become a coder")) //, "b3c0m3 4 c0d3r")
// console.log(hackerSpeak("hi there")) //, "h1 th3r3")
// console.log(hackerSpeak("programming is fun")) //, "pr0gr4mm1ng 15 fun")
// console.log(hackerSpeak("keep on practicing")) //, "k33p 0n pr4ct1c1ng")

// *<----------------------------------------------------------------------------->*/

// Create a function that takes a number num and returns its double factorial.

// function doubleFactorial(num) {
//     if (num <= 0) {
//         return 1;
//     }
//     return num * doubleFactorial(num - 2)
// }

// console.log(doubleFactorial(-1)) //, 1)
// console.log(doubleFactorial(0)) //, 1)
// console.log(doubleFactorial(1)) //, 1)
// console.log(doubleFactorial(2)) //, 2)
// console.log(doubleFactorial(7)) //, 105)
// console.log(doubleFactorial(9)) //, 945)
// console.log(doubleFactorial(14)) //, 645120)
// console.log(doubleFactorial(22)) //, 81749606400)
// console.log(doubleFactorial(25)) //, 7905853580625)
// console.log(doubleFactorial(27)) //, 213458046676875)

// *<----------------------------------------------------------------------------->*/

// Create a function that returns the mean of all digits.

// function mean(num) {
//     const s = Math.abs(num).toString().split("");
//     return s.reduce((a, c) => a + parseInt(c), 0) / s.length;
// }

// console.log(mean(666)) //, 6)
// console.log(mean(80)) //, 4)
// console.log(mean(789)) //, 8)
// console.log(mean(417)) //, 4)
// console.log(mean(1357)) //, 4)
// console.log(mean(42)) //, 3)
// console.log(mean(12345)) //, 3)

// *<----------------------------------------------------------------------------->*/


// Create a function that takes a number as its argument and 
// returns an array of all its factors.

// function factorize(num) {
//     let factors = []
//     for (let i = 1; i <= num; i++) {
//         if (num % i === 0) {
//             factors.push(i)
//         }
//     }
//     return factors
// }
// console.log(factorize(12)) //, [1, 2, 3, 4, 6, 12])
// console.log(factorize(4)) //, [1, 2, 4])
// console.log(factorize(17)) //, [1, 17])
// console.log(factorize(24)) //, [1, 2, 3, 4, 6, 8, 12, 24])
// console.log(factorize(1)) //, [1])

// *<----------------------------------------------------------------------------->*/

// Create a function that takes an array of items and checks if the
// last item matches the rest of the array.

// function matchLastItem(arr) {
//     let firstArr = arr.splice(0, arr.length - 1)
//     let secArr = arr.splice(arr.length - 1)
//     let str1 = firstArr.join('');
//     let str2 = secArr.join('')
//     return str1 === str2 ? true : false
// }

// console.log(matchLastItem(['rsq', '6hi', 'g', 'rsq6hig'])) //, true)
// console.log(matchLastItem([0, 1, 2, 3, 4, 5, '12345'])) //, false)
// console.log(matchLastItem(['for', 'mi', 'da', 'bel', 'formidable'])) //, false)
// console.log(matchLastItem([8, 'thunder', true, '8thundertrue'])) //, true)
// console.log(matchLastItem([1, 1, 1, '11'])) //, false)
// console.log(matchLastItem(['tocto', 'G8G', 'xtohkgc', '3V8', 'ctyghrs', 100.88,
// 'fyuo', 'Q', 'toctoG8Gxtohkgc3V8ctyghrs100.88fyuoQ'])) //, true)

/*----------------9/17/20  12:44 pM ---------------------*/

// Create a function that takes a word and returns true if the
// word has two consecutive identical letters.

// function doubleLetters(word) {
//     let same = word.split('').filter((a, index, word) => {
//         return word[index] == word[index + 1]
//     })
//     return same.length >= 1 ? true : false;
// }

// // True test cases
// console.log(doubleLetters("loop")) //, true)
// console.log(doubleLetters("meeting")) //, true)
// console.log(doubleLetters("yummy")) //, true)
// console.log(doubleLetters("moo")) //, true)
// console.log(doubleLetters("toodles")) //, true)
// console.log(doubleLetters("droop")) //, true)
// console.log(doubleLetters("loot")) //, true)
// // // False test cases
// console.log(doubleLetters("orange")) //, false)
// console.log(doubleLetters("munchkin")) //, false)
// console.log(doubleLetters("forestry")) //, false)
// console.log(doubleLetters("raindrops")) //, false)
// console.log(doubleLetters("gold")) //, false)
// console.log(doubleLetters("paradise")) //, false)
// console.log(doubleLetters("chicken")) //, false)

// *<----------------------------------------------------------------------------->*/