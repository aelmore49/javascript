
function add(num1, num2) {
    return num1 + num2;
}

console.log(add(666, 420))

function outer() {
    var b = 10;
    var c = 100;
    var a = 20;
    function inner() {
        //  var a = 20;
        console.log("a= " + a + " b= " + b);
        a++;
        b++;

    }
    return inner;
}

var X = outer();  // outer() invoked the first time
var Y = outer();  // outer() invoked the second time
//end of outer() function executions
X(); // X() invoked the first time
X(); // X() invoked the second time
X(); // X() invoked the third timeY(); // Y() invoked the first time

let userName = 'Alex'

function greetUser() {
    console.log('Hi ' + userName)
}
greetUser();

// function powerOf(x, y) {
//     let result = 1;
//     for (let i = 0; i < n; i++) {
//         result *= x
//     }
//     return result;
// }


function powerOf(x, n) {
    if (n === 1) {
        return x;
    }
    return x * powerOf(x, n - 1)
}

console.log(powerOf(2, 3))

const myself = {
    name: 'Alex',
    friends: [
        {
            name: 'Tim',
            friends: [
                {
                    name: 'Jack',
                    friends: [
                        {
                            name: 'Dougy'
                        }
                    ]
                }
            ]
        },
        {
            name: 'Jill'
        }
    ]
}

function printFriendNames(person) {
    const collectedNames = [];
    if (!person.friends) {
        return [];
    }
    for (const friend of person.friends) {
        collectedNames.push(friend.name)
        collectedNames.push(...printFriendNames(friend));
    }
    return collectedNames;
}

console.log(printFriendNames(myself))