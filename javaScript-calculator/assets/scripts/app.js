
const defualtResult = 0;
let currentResult = defualtResult;
let logEntries = [];

function getUserInput() {
    return parseInt(userInput.value);
}

function log(operator, resultBefore, calNum) {
    const description = `${resultBefore} ${operator} ${calNum}`;
    outputResult(currentResult, description);
}

function logEntry(operation, initialResult, enteredNumber, finalResult) {
    const logEntry = {
        logNumber: logEntries.length,
        operation: operation,
        startValue: initialResult,
        enteredValue: enteredNumber,
        finalResult: finalResult
    };
    logEntries.push(logEntry);
    console.log(logEntries)
}

function calculate(operation) {
    const enteredNumber = getUserInput();
    const initialResult = currentResult;
    let operator;
    if (operation === 'ADD') {
        currentResult += enteredNumber;
        operator = '+';
    } else if (operation === 'SUBTRACT') {
        currentResult -= enteredNumber;
        operator = '-'
    }
    else if (operation === 'MULTIPLIE') {
        currentResult *= enteredNumber;
        operator = '*'
    }
    else if (operation === 'DIVIDE') {
        currentResult /= enteredNumber;
        operator = '/'
    }
    log(operator, initialResult, enteredNumber);
    logEntry(operation, initialResult, enteredNumber, currentResult)
}

addBtn.addEventListener('click', calculate.bind(this,'ADD'));
subtractBtn.addEventListener('click', calculate.bind(this,'SUBTRACT'));
multiplyBtn.addEventListener('click', calculate.bind(this,'MULTIPLIE'));
divideBtn.addEventListener('click', calculate.bind(this,'DIVIDE'));


