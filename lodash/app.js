const customers = ['Max', 'Manuel', 'Anna'];

const activeCustomers = ['Max', 'Manuel'];

const inactiveCustomers = _.difference(customers, activeCustomers);


// Practice without lodash
//const inactiveCustomers = [];
// for (let i = 0; i < customers.length; i++) {
//     let match = false;
//     for (let j = 0; j < activeCustomers.length; j++) {
//         if (customers[i] === activeCustomers[j]) {
//             match = true;
//             break;
//         }
//     }
//     if (!match) {
//         inactiveCustomers.push(customers[i])
//     }
// }

console.log(inactiveCustomers)