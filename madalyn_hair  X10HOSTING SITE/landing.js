// Effects for the Top Hamburger Menu Buttons for the Landing Page

const menuBtn = document.querySelector(".menu-btn");
const menu = document.querySelector(".menu");
const menuNav = document.querySelector(".menu-nav");
const menuBranding = document.querySelector(".menu-branding");
const btnLines = document.querySelectorAll(".btn-line");
const navItems = document.querySelectorAll(".nav-item");
const sectionAbout = document.querySelectorAll("composition");
const sectionFeatures = document.querySelectorAll("section-features");

//Initial State of Landing Page

let showMenu = false;

menuBtn.addEventListener("click", toggleMenu);

function toggleMenu() {
  if (!showMenu) {
    menuBtn.classList.add("close");
    menu.classList.add("show");
    menuNav.classList.add("show");
    menuBranding.classList.add("show");
    btnLines.forEach(item => item.classList.add("close"));
    navItems.forEach(item => item.classList.add("show"));
    sectionAbout.forEach(item => item.classList.add("hide"));
    sectionFeatures.forEach(item => item.classList.add("hide"));

    // Set Menu State
    showMenu = true;
  } else {
    menuBtn.classList.remove("close");
    menu.classList.remove("show");
    menuNav.classList.remove("show");
    menuBranding.classList.remove("show");
    navItems.forEach(item => item.classList.remove("show"));
    btnLines.forEach(item => item.classList.remove("close"));
    sectionAbout.forEach(item => item.classList.remove("hide"));
    sectionFeatures.forEach(item => item.classList.remove("hide"));

    // Set Menu State back to false
    showMenu = false;
  }
}

function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}
function myFunction1() {
  document.getElementById("myDropdown1").classList.toggle("show");
}
function myFunction2() {
  document.getElementById("myDropdown2").classList.toggle("show");
}
function myFunction3() {
  document.getElementById("myDropdown3").classList.toggle("show");
}
function myFunction4() {
  document.getElementById("myDropdown4").classList.toggle("show");
}
function myFunction5() {
  document.getElementById("myDropdown5").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.addEventListener("click", function(e) {
  if (!e.target.matches(".dropbtn")) {
    var myDropdown = document.getElementById("myDropdown");
    if (myDropdown.classList.contains("show")) {
      myDropdown.classList.remove("show");
    }
  }
});

window.addEventListener("click", function(e) {
  if (!e.target.matches(".dropbtn1")) {
    var myDropdown1 = document.getElementById("myDropdown1");
    if (myDropdown1.classList.contains("show")) {
      myDropdown1.classList.remove("show");
    }
  }
});

window.addEventListener("click", function(e) {
  if (!e.target.matches(".dropbtn2")) {
    var myDropdown2 = document.getElementById("myDropdown2");
    if (myDropdown2.classList.contains("show")) {
      myDropdown2.classList.remove("show");
    }
  }
});

window.addEventListener("click", function(e) {
  if (!e.target.matches(".dropbtn3")) {
    var myDropdown3 = document.getElementById("myDropdown3");
    if (myDropdown3.classList.contains("show")) {
      myDropdown3.classList.remove("show");
    }
  }
});

window.addEventListener("click", function(e) {
  if (!e.target.matches(".dropbtn4")) {
    var myDropdown4 = document.getElementById("myDropdown4");
    if (myDropdown4.classList.contains("show")) {
      myDropdown4.classList.remove("show");
    }
  }
});

window.addEventListener("click", function(e) {
  if (!e.target.matches(".dropbtn5")) {
    var myDropdown5 = document.getElementById("myDropdown5");
    if (myDropdown5.classList.contains("show")) {
      myDropdown5.classList.remove("show");
    }
  }
});
