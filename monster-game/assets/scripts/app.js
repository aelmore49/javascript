


const ATTACK_VALUE = 10;
const MONSTER_ATTACK_VALUE = 14;
const STRONG_ATTACK_VALUE = 17;
const HEAL_VALUE = 8;
const MODE_ATTACK = 'ATTACK';
const MODE_STRONG = 'STRONG_ATTACK';
const LOG_EVENT_PLAYER_ATTACK = 'PLAYER_ATTACK';
const LOG_EVENT_PLAYER_STRONG_ATTACK = 'PLAYER_STRONG_ATTACK';
const LOG_EVENT_MONSTER_ATTACK = 'MONSTER_ATTACK';
const LOG_EVENT_PLAYER_HEAL = 'PLAYER_HEAL';
const LOG_EVENT_GAME_OVER = 'GAME_OVER';

let chosenMaxLife;

try {
    chosenMaxLife = enteredHealth();
} catch (error) {
    console.log(error.message);
    chosenMaxLife = 100;
}
adjustHealthBars(chosenMaxLife);

let currentMonsterHealth = chosenMaxLife;
let currentPlayerHealth = chosenMaxLife;
let hasBonusLife = true;

let battleLog = []

function writeToLog(event, value, monsterHealth, playerHealth) {
    let logEntry = {
        event: event,
        value: value,
        monsterHealth: monsterHealth,
        playerHealth: playerHealth
    }
    switch (event) {
        case LOG_EVENT_PLAYER_ATTACK:
            logEntry.target = 'MONSTER SUFFERED ATTACKED FROM PLAYER';
            break;
        case LOG_EVENT_PLAYER_STRONG_ATTACK:
            logEntry.target = 'MONSTER SUFFERED STRONG ATTACK FROM PLAYER';
            break;
        case LOG_EVENT_MONSTER_ATTACK:
            logEntry.target = 'PLAYER SUFFERED ATTACK FROM MONSTER';
            break;
        case LOG_EVENT_PLAYER_HEAL:
            logEntry.target = 'PLAYER HEALD THEMSELVES';
            break;
        default:
            logEntry = {};
    }
    battleLog.push(logEntry)
}

function enteredHealth() {
    let response = prompt(`Please enter the amount of health you want for the game. 
    Must be a number greater than 0 and less than 100.`,
        '100');
    const parsedValue = parseInt(response)
    if (isNaN(parsedValue) || parsedValue <= 0 || parsedValue > 100) {
        throw { message: 'Invalid user input, not a number.' };
    }
    return parsedValue
}
function reset() {
    chosenMaxLife = enteredHealth()
    currentMonsterHealth = chosenMaxLife;
    currentPlayerHealth = chosenMaxLife;
    resetGame(chosenMaxLife)
}

function endRound() {
    const initialPlayerHealth = currentPlayerHealth;
    const playerDamge = dealPlayerDamage(MONSTER_ATTACK_VALUE);
    currentPlayerHealth -= playerDamge;
    writeToLog(LOG_EVENT_MONSTER_ATTACK, playerDamge, currentMonsterHealth, currentPlayerHealth);

    if (currentPlayerHealth <= 0 && hasBonusLife) {
        hasBonusLife = false;
        removeBonusLife();
        currentPlayerHealth = initialPlayerHealth;
        setPlayerHealth(initialPlayerHealth);
        alert('You were saved by the bonus life!');
    }
    if (currentMonsterHealth <= 0 && currentPlayerHealth > 0) {
        alert('You won!');
        writeToLog(LOG_EVENT_MONSTER_ATTACK, 'PLAYER WON', currentMonsterHealth, currentPlayerHealth);
        reset();
    } else if (currentPlayerHealth <= 0 && currentMonsterHealth > 0) {
        alert('You dead, bitch!');
        writeToLog(LOG_EVENT_MONSTER_ATTACK, 'MONSTER WON', currentMonsterHealth, currentPlayerHealth);
        reset();
    }
    else if (currentPlayerHealth <= 0 && currentMonsterHealth <= 0) {
        alert('You fools had a draw!');
        writeToLog(LOG_EVENT_MONSTER_ATTACK, 'IT WAS A DRAW', currentMonsterHealth, currentPlayerHealth);
        reset();
    }
}

function attackMonster(mode) {
    const maxDamage = mode === MODE_ATTACK ? ATTACK_VALUE : STRONG_ATTACK_VALUE;
    const logEvent = mode === MODE_ATTACK ? LOG_EVENT_PLAYER_ATTACK : LOG_EVENT_PLAYER_STRONG_ATTACK;
    const damage = dealMonsterDamage(maxDamage);
    currentMonsterHealth -= damage;
    writeToLog(logEvent, damage, currentMonsterHealth, currentPlayerHealth);
    endRound()
}

function attackHandler() {
    attackMonster(MODE_ATTACK);
}

function strongAttackHandler() {
    attackMonster(MODE_STRONG);
}

function healPlayerHander() {
    let healValue;
    if (currentPlayerHealth >= chosenMaxLife - HEAL_VALUE) {
        alert("You can't heal more than 100%")
        healValue = chosenMaxLife - currentPlayerHealth;
    } else {
        healValue = HEAL_VALUE;
    }
    increasePlayerHealth(healValue);
    currentPlayerHealth += healValue;
    writeToLog(LOG_EVENT_PLAYER_HEAL, healValue, currentMonsterHealth, currentPlayerHealth);
    endRound()
}

function printLogHandler() {

    // Access log using forEach loop
    console.log(battleLog.forEach((element, index) => {
        console.log('---------------------')
        console.log(
            'Turn:', index + 1,
            'Value:', element.value,
            'Event:', element.event,
            'Monster Health:', element.monsterHealth,
            'Player Health:', element.playerHealth);
    }))


    /* Access log using for loop */
    // for (let i = 0; i < battleLog.length; i++) {
    //     console.log('----------------------------------');
    //     console.log(battleLog[i]);
    // }


    // Access log using for of loop with nested for in loop
    // let i = 0;
    // for (const el of battleLog) {
    //     console.log(`#${i}`);
    //     for (const key in el) {
    //        console.log(`Key Name: ${key} -> Value: ${el[key]}`)
    //     }
    //     i++;
    // }

}

attackBtn.addEventListener('click', attackHandler);
strongAttackBtn.addEventListener('click', strongAttackHandler);
healBtn.addEventListener('click', healPlayerHander);
logBtn.addEventListener('click', printLogHandler);