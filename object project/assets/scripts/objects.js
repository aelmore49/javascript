

const addMovieBtn = document.getElementById('add-movie-btn');
const searchBtn = document.getElementById('search-btn');
const movies = [];

const renderMovies = (filter = '') => {
    const movieList = document.getElementById('movie-list');

    if (!movies.length) {
        movieList.classList.remove('visible');
        return;
    } else {
        movieList.classList.add('visible');
    }
    movieList.innerHTML = ''
    const filteredMovies = !filter ? movies : movies.filter(movie => movie.info.title.includes(filter))
    filteredMovies.forEach((el) => {
        const { info, ...otherProps } = el;
        console.log(otherProps);
        const { getFormattedTitle } = el;
        let text = getFormattedTitle.call(el) + '-';
        for (e in info) {
            if (e !== 'title') {
                text = text + `${e}: ${info[e]}`
            }
        }
        const movieNode = document.createElement('li');
        movieNode.textContent = text;
        movieList.appendChild(movieNode);
    })
};

const addMovieHandler = () => {
    const title = document.getElementById('title').value;
    const extraName = document.getElementById('extra-name').value;
    const extraValue = document.getElementById('extra-value').value;
    if (title.trim() === '' || extraName.trim() === '', extraValue.trim() === '') {
        alert('Please enter all fields of for your favorite movie');
        return;
    }
    const newMovie = {
        info: {
            title: title,
            [extraName]: extraValue
        },
        id: Math.random().toString(),
        getFormattedTitle: function () {
            console.log(this.info.title)
            return this.info.title.toUpperCase();
        }
    }
    movies.push(newMovie);

    renderMovies();
};

const searchMovieHandler = () => {
    const filterTerm = document.getElementById('filter-title').value;
    renderMovies(filterTerm);
};

searchBtn.addEventListener('click', searchMovieHandler);
addMovieBtn.addEventListener('click', addMovieHandler);