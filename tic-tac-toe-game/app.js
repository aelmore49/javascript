
const startGameBtn = document.getElementById('start-game-btn');
const showStatsBtn = document.getElementById('show-stats-btn')
const ROCK = 'ROCK';
const PAPER = 'PAPER';
const SCISSORS = 'SCISSORS';
const DEFAULT_USER_CHOICE = 'ROCK';
const RESULT_DRAW = 'You tied.';
const RESULT_PLAYER_WINS = 'You won!';
const RESULT_COMPUTER_WINS = 'You lost!';

const gameLog = [];
let gameStats = {
    result: '',
    playerWins: 0,
    pcWins: 0,
    draws: 0
};

let gameIsRunning = false;

const getPlayerChoice = () => {
    const selection = prompt(`Please choice ${ROCK}, ${PAPER} or ${SCISSORS}.`, '').toUpperCase();

    if (
        selection !== ROCK &&
        selection !== PAPER &&
        selection !== SCISSORS) {

        alert('Because your choice was invalid, we choice Rock for you. ');
        return DEFAULT_USER_CHOICE;
    }
    alert(`You choose ${selection}.`);

    return selection;
};

const getComputerChoice = () => {
    const randomValue = Math.random();
    if (randomValue < .34) {
        alert(`Computer choice ${ROCK}`);
        return ROCK;
    } else if (randomValue < .687) {
        alert(`Computer choice ${PAPER}`)
        return PAPER;

    } else {
        alert(`Computer choice ${SCISSORS}`)
        return SCISSORS;
    }
};

const getWinner = (pChoice, cChoice) =>
    cChoice === pChoice
        ? RESULT_DRAW
        : (cChoice === ROCK && pChoice === SCISSORS) ||
            (cChoice === SCISSORS && pChoice === PAPER) ||
            (cChoice === PAPER && pChoice === ROCK)
            ? RESULT_COMPUTER_WINS
            : RESULT_PLAYER_WINS;

startGameBtn.addEventListener('click', function () {
    if (gameIsRunning) {
        return;
    }
    gameIsRunning = true;
    const playerSelection = getPlayerChoice();
    const computerSelection = getComputerChoice();
    const winner = getWinner(playerSelection, computerSelection);
    let message = `You picked ${playerSelection} and the computer picked ${computerSelection},`;
    if (winner === RESULT_DRAW) {
        gameStats.result = 'Match tied'
        gameStats.draws++;
        message += ` therefore you had a draw.`;
    } else if (winner === RESULT_PLAYER_WINS) {
        gameStats.playerWins++;
        gameStats.result = 'Player won';
        message += ` therefore you Won! =^)`;
    } else if (winner === RESULT_COMPUTER_WINS) {
        gameStats.result = 'Computer won'
        gameStats.pcWins++;
        message += ` therefore you lost. =^( `;
    }
    gameLog.push(gameStats);
    alert(message);
    gameIsRunning = false;

});

showStatsBtn.addEventListener('click', function () {
    let message = '';
    message += ` 
     Current Game Number: ${gameLog.length}
     Result: ${gameStats.result}
     -----------------------------------------
     Total Number of Games Played: ${gameLog.length}
     Total Wins: ${gameStats.playerWins} 
     Total Losses: ${gameStats.pcWins}
     Total Draws: ${gameStats.draws}

     `
    if (gameLog.length) {
        alert(message);
    } else {
        alert('There are no stats because no games have been played yet.');
    }
})
